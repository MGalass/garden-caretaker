#ifndef __HYGROMETER__
#define __HYGROMETER__

class Hygrometer { 
public:
  Hygrometer(int vccPin, int dataPin);
  float getSoilHumidity();
private:
  int vccPin;
  int dataPin;
};

#endif
