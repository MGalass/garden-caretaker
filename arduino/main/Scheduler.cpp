#include "Scheduler.h"
#include "Arduino.h"

//#define DEBUG 1

void Scheduler::init(unsigned long basePeriod){
  this->basePeriod = basePeriod;
  timer.setupPeriod(basePeriod);  
  nTasks = 0;
}

bool Scheduler::addTask(Task* task){
  if (nTasks < MAX_SCHEDULER_TASKS - 1){
    taskList[nTasks] = task;
    #ifdef DEBUG
    Serial.println("Task added at position: " + String(nTasks));
    #endif
    nTasks++;
    return true;
  } else {
    #ifdef DEBUG
    Serial.println("The scheduler is full");
    #endif
    return false; 
  }
}
  
void Scheduler::schedule(){
  #ifdef DEBUG
  Serial.println("Scheduling nTask = " + String(nTasks));
  #endif

  timer.waitForNextTick();
  for (int i = 0; i < nTasks; i++){
    if (taskList[i]->updateAndCheckTime(basePeriod)){
        taskList[i]->tick();
      }
    }
}

