#ifndef __RAIN_SENSOR__
#define __RAIN_SENSOR__

class RainSensor { 
public:
  RainSensor(int vccPin, int analogPin, int digitalPin);
  float getRainValue();
private:
  int vccPin;
  int analogPin;
  int digitalPin;
};

#endif
