#ifndef __TIMER__
#define __TIMER__

class Timer {
    
public:  
  Timer();
  void setupFreq(unsigned long freq);  
  void setupPeriod(unsigned long period);  
  void waitForNextTick();
};


#endif

