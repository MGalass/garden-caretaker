#ifndef __TASK__
#define __TASK__

class Task {
  unsigned long taskPeriod;
  unsigned long timeElapsed;
  
public:
  virtual void init(unsigned long period){
    taskPeriod = period;  
    timeElapsed = 0;
  }

  virtual void tick() = 0;

  bool updateAndCheckTime(unsigned long basePeriod){
    timeElapsed += basePeriod;
    if (timeElapsed >= taskPeriod){
      timeElapsed = 0;
      return true;
    } else {
      return false; 
    }
  }

  unsigned long getPeriod() {
    return taskPeriod;
  }
  
};

#endif


