#ifndef __BRIGHTNESSTRACKERTASK__
#define __BRIGHTNESSTRACKERTASK__

#include "Task.h"
#include "SharedContext.h"
#include "LDR.h"

class BrightnessTrackerTask : public Task {
	public:
		BrightnessTrackerTask(SharedContext* context, int vccPin, int dataPin);
		void init(unsigned long period);
		void tick();
	private:
		SharedContext* context;
		LDR* sensor;
};

#endif
