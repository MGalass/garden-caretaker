#include "RainSensor.h"
#include "Arduino.h"
#include "Constants.h"

RainSensor::RainSensor(int vccPin, int analogPin, int digitalPin) {
  this->vccPin = vccPin;
  this->analogPin = analogPin;
  this->digitalPin = digitalPin;

  // Vcc pin set as output and high
  pinMode(vccPin, OUTPUT);
  digitalWrite(vccPin, HIGH);

  // Digital Output pin
  pinMode(digitalPin, OUTPUT);

  pinMode(analogPin, INPUT);
}

float RainSensor::getRainValue() {
  return analogRead(this->analogPin);
}

