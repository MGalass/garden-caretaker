#ifndef __CONSTANTS__
#define __CONSTANTS__

//Max lenght of a message
const int MAX_MESSAGE_LENGTH = 255;

const int MAX_SCHEDULER_TASKS = 10;

#endif
