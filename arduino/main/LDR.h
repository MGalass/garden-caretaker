#ifndef __LDR__
#define __LDR__

class LDR { 
public:
  LDR(int vccPin, int dataPin);
  float getBrightness();
private:
  int vccPin;
  int dataPin;
};

#endif
