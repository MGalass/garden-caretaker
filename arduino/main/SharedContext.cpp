#include "SharedContext.h"

SharedContext::SharedContext(MsgService* msgService) {
	this->msgService = msgService;
}

MsgService* SharedContext::getMsgService() {
	return msgService;
}