#include "Arduino.h"
#include "SharedContext.h"
#include "Constants.h"
#include "Scheduler.h"
#include "MsgService.h"
#include "BrightnessTrackerTask.h"
#include "TemperatureTrackerTask.h"
#include "SoilHumidityTrackerTask.h"
#include "RainfallTrackerTask.h"

// BrightessTaskTracker pins
#define BRIGHTNESS_VCC_PIN 8
#define BRIGHTNESS_AO_PIN 2
// TemperatureTrackerTask pins
#define TEMPERATURE_DO_PIN 2
// SoilHumidityTrackerTask pins
#define SOIL_HUMIDITY_VCC_PIN 7
#define SOIL_HUMIDITY_AO_PIN 0
// RainfallTrackerTask pins
#define RAINFALL_VCC_PIN 12
#define RAINFALL_AO_PIN 1
#define RAINFALL_DO_PIN 4

Scheduler scheduler;

void setup() {
  MsgService* msgService = new MsgService();
  msgService->init();

  Serial.begin(9600);
  while(!Serial){}
  msgService->sendMsg(Msg("- Ready"));

  SharedContext* context = new SharedContext(msgService);
  scheduler.init(1000);

  BrightnessTrackerTask* brightnessTrackerTask = new BrightnessTrackerTask(context, BRIGHTNESS_VCC_PIN, BRIGHTNESS_AO_PIN);
  brightnessTrackerTask->init(160000);
  scheduler.addTask(brightnessTrackerTask);

  TemperatureTrackerTask* temperatureTrackerTask = new TemperatureTrackerTask(context, TEMPERATURE_DO_PIN);
  temperatureTrackerTask->init(40000);
  scheduler.addTask(temperatureTrackerTask);

  SoilHumidityTrackerTask* soilHumidityTrackerTask = new SoilHumidityTrackerTask(context, SOIL_HUMIDITY_VCC_PIN, SOIL_HUMIDITY_AO_PIN);
  soilHumidityTrackerTask->init(190000);
  scheduler.addTask(soilHumidityTrackerTask);

  RainfallTrackerTask* rainfallTrackerTask = new RainfallTrackerTask(context, RAINFALL_VCC_PIN, RAINFALL_AO_PIN, RAINFALL_DO_PIN);
  rainfallTrackerTask->init(70000);
  scheduler.addTask(rainfallTrackerTask);
}

void loop() {
  // Start scheduling tasks based on their period
  scheduler.schedule();
}
