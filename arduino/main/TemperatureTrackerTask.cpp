#include "TemperatureTrackerTask.h"
#include "Arduino.h"
#include "OutputMessages.h"
#include "MsgService.h"

TemperatureTrackerTask::TemperatureTrackerTask(SharedContext* context, int dataPin) {
	this->context = context;
	this->sensor = new DHT(dataPin, DHTTYPE);
    this->sensor->begin();
}

void TemperatureTrackerTask::init(unsigned long period) {
	Task::init(period);
}

void TemperatureTrackerTask::tick() {
	//Retrieve the temperature from the sensor
	float temperature = this->sensor->readTemperature();
	context->getMsgService()->sendMsg(Msg(TEMPERATURE_KEY + String(temperature)));
}
