#include "LDR.h"
#include "Arduino.h"
#include "Constants.h"

LDR::LDR(int vccPin, int dataPin) {
  this->vccPin = vccPin;
  this->dataPin = dataPin;

  // Vcc pin set as output and high
  pinMode(vccPin, OUTPUT);
  digitalWrite(vccPin, HIGH);

  pinMode(dataPin, INPUT);
}

float LDR::getBrightness() {
  return analogRead(this->dataPin);
}

