#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include "Arduino.h"

#ifndef __MSG__
#define __MSG__
class Msg {
  String content;

public:
  Msg(const String& content){
    this->content = content;
  }
  
  String getContent(){
    return content;
  }
};
#endif

class MsgService {
    
public:   
  void init();  
  bool isMsgAvailable();
  Msg* receiveMsg();
  void sendMsg(Msg msg);

private:
  String content;  
};

#endif

