#ifndef __RAINFALLTRACKERTASK__
#define __RAINFALLTRACKERTASK__

#include "Task.h"
#include "SharedContext.h"
#include "RainSensor.h"

class RainfallTrackerTask : public Task {
	public:
		RainfallTrackerTask(SharedContext* context, int vccPin, int analogPin, int digitalPin);
		void init(unsigned long period);
		void tick();
	private:
		SharedContext* context;
		RainSensor* sensor;
};

#endif
