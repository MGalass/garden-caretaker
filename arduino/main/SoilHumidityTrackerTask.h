#ifndef __SOILHUMIDITYTRACKERTASK__
#define __SOILHUMIDITYTRACKERTASK__

#include "Task.h"
#include "SharedContext.h"
#include "Hygrometer.h"

class SoilHumidityTrackerTask : public Task {
	public:
		SoilHumidityTrackerTask(SharedContext* context, int vccPin, int dataPin);
		void init(unsigned long period);
		void tick();
	private:
		SharedContext* context;
		Hygrometer* sensor;
};

#endif
