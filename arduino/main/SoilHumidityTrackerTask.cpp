#include "SoilHumidityTrackerTask.h"
#include "Arduino.h"
#include "OutputMessages.h"
#include "MsgService.h"

SoilHumidityTrackerTask::SoilHumidityTrackerTask(SharedContext* context, int vccPin, int dataPin) {
	this->context = context;
	this->sensor = new Hygrometer(vccPin, dataPin);
}

void SoilHumidityTrackerTask::init(unsigned long period) {
	Task::init(period);
}

void SoilHumidityTrackerTask::tick() {
	//Retrieve the soil humidity from the sensor
	float soilHumidity = sensor->getSoilHumidity();
	context->getMsgService()->sendMsg(Msg(SOIL_HUMIDITY_KEY + String(soilHumidity)));
}
