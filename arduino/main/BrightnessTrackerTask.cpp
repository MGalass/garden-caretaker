#include "BrightnessTrackerTask.h"
#include "Arduino.h"
#include "OutputMessages.h"
#include "MsgService.h"

BrightnessTrackerTask::BrightnessTrackerTask(SharedContext* context, int vccPin, int dataPin) {
	this->context = context;
	this->sensor = new LDR(vccPin, dataPin);
}

void BrightnessTrackerTask::init(unsigned long period) {
	Task::init(period);
}

void BrightnessTrackerTask::tick() {
	//Retrieve the brightness from the sensor
	float brightness = sensor->getBrightness();
	context->getMsgService()->sendMsg(Msg(BRIGHTNESS_KEY + String(brightness)));
}
