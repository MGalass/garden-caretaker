#include "Arduino.h"
#include "MsgService.h"
#include "Constants.h"

void MsgService::init(){
  content.reserve(256);
  Serial.begin(9600);
  while (!Serial){}
}

void MsgService::sendMsg(Msg msg){
  String content = msg.getContent();
  Serial.println(content);
}

bool MsgService::isMsgAvailable(){
  return Serial.available();
}

Msg* MsgService::receiveMsg(){
  if (Serial.available()){
    int checkVar = 0; // Interrupt loop if the cicle runs for too long   
    content = "";
    int size = Serial.read();
    int nDataRec = 0;
    while (nDataRec < size && checkVar < MAX_MESSAGE_LENGTH) {
      checkVar++;
      if (Serial.available()){
        content += (char)Serial.read();      
        nDataRec++;
      }
    }
    return new Msg(content);
  } else {
    return NULL;  
  }
}




