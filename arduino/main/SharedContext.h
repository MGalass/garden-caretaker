#ifndef __SHAREDCONTEXT__
#define __SHAREDCONTEXT__

#include "MsgService.h"

class SharedContext {
	public:
		SharedContext(MsgService *msgService);
		MsgService* getMsgService();
	private:
		MsgService* msgService;
};

#endif
