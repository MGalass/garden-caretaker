#ifndef __OUTPUTMESSAGES__
#define __OUTPUTMESSAGES__

#include "Arduino.h"

  const String BRIGHTNESS_KEY = "B";
  const String TEMPERATURE_KEY = "T";
  const String SOIL_HUMIDITY_KEY = "S";
  const String RAINFALL_KEY = "R";

#endif
