#include "Hygrometer.h"
#include "Arduino.h"
#include "Constants.h"

Hygrometer::Hygrometer(int vccPin, int dataPin) {
  this->vccPin = vccPin;
  this->dataPin = dataPin;

  // Vcc pin set as output and high
  pinMode(vccPin, OUTPUT);
  digitalWrite(vccPin, HIGH);

  //pinMode(dataPin, INPUT);
}

float Hygrometer::getSoilHumidity() {
  return analogRead(this->dataPin);
}

