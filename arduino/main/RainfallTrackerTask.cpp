#include "RainfallTrackerTask.h"
#include "Arduino.h"
#include "OutputMessages.h"
#include "MsgService.h"

RainfallTrackerTask::RainfallTrackerTask(SharedContext* context, int vccPin, int analogPin, int digitalPin) {
	this->context = context;
	this->sensor = new RainSensor(vccPin, analogPin, digitalPin);
}

void RainfallTrackerTask::init(unsigned long period) {
	Task::init(period);
}

void RainfallTrackerTask::tick() {
	//Retrieve the rainfall value from the sensor
	float rainfall = sensor->getRainValue();
	context->getMsgService()->sendMsg(Msg(RAINFALL_KEY + String(rainfall)));
}
