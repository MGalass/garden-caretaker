#ifndef __TEMPERATURETRACKERTASK__
#define __TEMPERATURETRACKERTASK__

#include "Task.h"
#include "SharedContext.h"

#include "DHT.h"
#define DHTTYPE DHT11

class TemperatureTrackerTask : public Task {
	public:
		TemperatureTrackerTask(SharedContext* context, int dataPin);
		void init(unsigned long period);
		void tick();
	private:
		SharedContext* context;
		DHT* sensor;
};

#endif
