#define VCC_PIN 7
#define DATA_PIN 0

void setup() {
  // Pin set as output
  pinMode(VCC_PIN, OUTPUT);
  digitalWrite(VCC_PIN, HIGH);

  Serial.begin(9600);
}

void loop() {
  int value = analogRead(DATA_PIN);
  Serial.println(value);
  delay(1000);
}
