#define VCC_PIN 12
#define DO_PIN 4
#define AO_PIN 1

void setup() {
  // Pin set as output for vcc
  pinMode(VCC_PIN, OUTPUT);
  digitalWrite(VCC_PIN, HIGH);

  // Digital Output pin
  pinMode(DO_PIN, OUTPUT);

  Serial.begin(9600);
}

void loop() {
  int value = analogRead(AO_PIN);
  Serial.println(value);
  delay(3000);
}
