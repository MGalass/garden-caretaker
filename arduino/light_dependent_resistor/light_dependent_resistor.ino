#define VCC_PIN 8
#define AO_PIN 2

void setup() {
  // Pin set as output for vcc
  pinMode(VCC_PIN, OUTPUT);
  digitalWrite(VCC_PIN, HIGH);
  
  Serial.begin(9600);

}

void loop() {
  int value = analogRead(AO_PIN);
  Serial.println(value);
  delay(3000);
}
