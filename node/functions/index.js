const BRIGHTNESS_LOW_VALUE = 400
const BRIGHTNESS_HIGH_VALUE = 900
const SOIL_HUMIDITY_WATER_VALUE = 355 // In water
const SOIL_HUMIDITY_AIR_VALUE = 734 // In open air
const SOIL_HUMIDITY_DELTA = 100

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

const havePlantSensorDataChanged = function(previousdata, newData) {
    return (previousdata && newData && 
            ((previousdata.temperatureLastValue && newData.temperatureLastValue && previousdata.temperatureLastValue != newData.temperatureLastValue)
            || (previousdata.brightnessLastValue && newData.brightnessLastValue && previousdata.brightnessLastValue != newData.brightnessLastValue)
            || (previousdata.rainfallLastValue && newData.rainfallLastValue && previousdata.rainfallLastValue != newData.rainfallLastValue)
            || (previousdata.soilHumidityLastValue && newData.soilHumidityLastValue && previousdata.soilHumidityLastValue != newData.soilHumidityLastValue))
        );
}

const mustWarnTemperature = function(temperatureMinimum, temperatureLastValue) {
    if (!temperatureMinimum || !temperatureLastValue) {
        return false;
    }
    return temperatureLastValue < temperatureMinimum;
}

const mustWarnBrightness = function(recommendedBrightness, brightnessLastValue) {
    if (!recommendedBrightness || !brightnessLastValue) {
        return false;
    }
    if (recommendedBrightness == "Medium") {
        return brightnessLastValue < BRIGHTNESS_LOW_VALUE || brightnessLastValue > BRIGHTNESS_HIGH_VALUE;
    } else if (recommendedBrightness == "High") {
        return brightnessLastValue < BRIGHTNESS_HIGH_VALUE;
    } else {
        return brightnessLastValue > BRIGHTNESS_LOW_VALUE;
    }
}

const mustWarnSoilHumidity = function(recommendedSoilHumidity, soilHumidityLastValue) {
    if (!recommendedSoilHumidity || !soilHumidityLastValue) {
        return false;
    }
    if (recommendedSoilHumidity == "Medium") {
        return soilHumidityLastValue > SOIL_HUMIDITY_AIR_VALUE - SOIL_HUMIDITY_DELTA || soilHumidityLastValue < SOIL_HUMIDITY_WATER_VALUE + SOIL_HUMIDITY_DELTA;
    } else if (recommendedSoilHumidity == "High") {
        return soilHumidityLastValue > SOIL_HUMIDITY_WATER_VALUE + SOIL_HUMIDITY_DELTA;
    } else {
        return soilHumidityLastValue < SOIL_HUMIDITY_AIR_VALUE - SOIL_HUMIDITY_DELTA;
    }
}

const mustWarnRainfall = function(precipitationMinimum, precipitationMaximum, rainfallLastValue) {
    if (!rainfallLastValue || (!precipitationMinimum && !precipitationMaximum)) {
        return false;
    }
    if (precipitationMinimum) {
        if (precipitationMaximum) {
            // Both value
            return rainfallLastValue < precipitationMinimum || rainfallLastValue > precipitationMaximum
        } else {
            // No maximum
            return rainfallLastValue < precipitationMinimum 
        }
    } else {
        // No minimum
        return rainfallLastValue > precipitationMaximum 
    }
}

exports.checkPlantSensorValues = functions.firestore
    .document('plants/{plantId}')
    .onUpdate((change, context) => {
        const previousData = change.before.data();
        const newData = change.after.data();

        if (!havePlantSensorDataChanged(previousData, newData)) {
            // Prevent loops
            return null;
        }

        const plantId = context.params.plantId;
        const userId = newData.user;
        const areaId = newData.area;

        console.log(userId + ' - ' + areaId + ' - ' + plantId);

        const mustSetWarning = (mustWarnTemperature(newData.temperatureMinimum, newData.temperatureLastValue)
                                || mustWarnBrightness(newData.recommendedBrightness, newData.brightnessLastValue)
                                || mustWarnSoilHumidity(newData.recommendedSoilHumidity, newData.soilHumidityLastValue)
                                || (mustWarnRainfall(newData.precipitationMinimum, newData.precipitationMaximum, newData.rainfallLastValue) && false));

        const plantQuery = {
            [`plants.${plantId}.warning`] : mustSetWarning
        };

        console.log(plantQuery);

        // Update plant info in user area
        const fs = admin.firestore();
        fs.collection('users').doc(userId).collection('areas').doc(areaId).update(plantQuery);

        // Update plant info in area
        fs.collection('areas').doc(areaId).update(plantQuery);

        // Update plant info in plants
        return change.after.ref.set({
            warning: mustSetWarning
        }, {merge: true});
    });

const mustSendNotificationOnWarning = function(previousdata, newData) {
    // Send a notification if the 'warning' field is set to true and if the field has actually changed (avoid loops)
    return (previousdata && newData && newData.warning && previousdata.warning != newData.warning);
}

exports.sendNotificationOnWarning = functions.firestore
    .document('plants/{plantId}')
    .onUpdate((change, context) => {
        const previousData = change.before.data();
        const newData = change.after.data();

        if (!mustSendNotificationOnWarning(previousData, newData)) {
            // Prevent loops
            return null;
        }

        const userId = newData.user;

        const messaging = admin.messaging();

        console.log(`Sending notification to user ${userId}`);

        return messaging.sendToTopic(`user-${userId}`, {
            notification: {
                title: 'Warning',
                body: `Your plant [${newData.name}] requires assistance`
              },
        }).then(function(value, error) {
            if (error) {
                console.log('Notification could not be sent: ' + error);
            }

            console.log(`Notification sent to user: ${userId}`);

            return null;
        })
    });

