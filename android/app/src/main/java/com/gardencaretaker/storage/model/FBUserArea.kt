package com.gardencaretaker.storage.model

data class FBUserArea (
    var id: String? = "",
    var name: String? = "",
    var type: String? = "",
    var plants: Map<String, FBAreaPlant>? = HashMap()
    //var plants: List<FBAreaPlant>? = ArrayList()
) { }

