package com.gardencaretaker.storage.model

data class FBUser(
    var userId: String? = "",
    var username: String? = "",
    var areas: List<FBUserArea>? = ArrayList()
) { }