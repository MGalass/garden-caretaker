package com.gardencaretaker.storage.model

import com.gardencaretaker.model.Plant

data class FBAreaPlant(
    var id: String? = "",
    var active: Boolean? = false,
    var warning: Boolean? = false,
    var name: String? = "",
    var species: String? = "",
    var apiId: Int? = null,

    var recommendedBrigthness: String? = Plant.DEFAULT_BRIGHTNESS,
    var recommendedSoilHumidity: String? = Plant.DEFAULT_SOIL_HUMIDITY,
    var precipitationMinimum: Double? = null,
    var precipitationMaximum: Double? = null,
    var temperatureMinimum: Double? = null
) { }

