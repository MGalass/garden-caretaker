package com.gardencaretaker.storage

import android.app.Activity
import android.content.SharedPreferences
import com.gardencaretaker.GardenCaretaker

object UserPreferences {

    val KEY_USER_PREFS = "keyUserPrefs"
    val KEY_USER_ID = "keyUserId"
    val KEY_USER_USERNAME = "keyUserUsername"

    private val sp = GardenCaretaker.instance.applicationContext.getSharedPreferences(KEY_USER_PREFS, Activity.MODE_PRIVATE)

    fun putUserId(userId: String) {
        sp.edit()
            .putString(KEY_USER_ID, userId)
            .apply()
    }

    fun getUserId(): String {
        return sp.getString(KEY_USER_ID, "")!!
    }

    fun putUsername(username: String) {
        sp.edit()
            .putString(KEY_USER_USERNAME, username)
            .apply()
    }

    fun getUsername(): String {
        return sp.getString(KEY_USER_USERNAME, "")!!
    }

    fun clear() {
        sp.edit()
            .clear()
            .apply()
    }

}