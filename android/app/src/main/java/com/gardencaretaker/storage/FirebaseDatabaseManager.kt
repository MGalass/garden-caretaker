package com.gardencaretaker.storage

import android.text.TextUtils
import android.util.Log
import com.gardencaretaker.model.Area
import com.gardencaretaker.model.Plant
import com.gardencaretaker.model.User
import com.gardencaretaker.utils.GenericResponse
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

object FirebaseDatabaseManager {

    private val TAG = "FDM"

    val USERS = "users"
    val AREAS = "areas"
    val PLANTS = "plants"

    private var fireStore: FirebaseFirestore

    init {
        fireStore = Firebase.firestore
    }

    // Base collections references

    fun usersReference(): CollectionReference {
        return fireStore.collection(USERS)
    }

    fun areasReference(): CollectionReference {
        return fireStore.collection(AREAS)
    }

    fun plantsReference(): CollectionReference {
        return fireStore.collection(PLANTS)
    }

    // Setters

    fun putUser(user: User): Task<Void> {
        // Add user info
        return usersReference().document(user.userId!!).set(user.toFBUser())
    }

    fun putUserArea(userUid: String, area: Area): Task<GenericResponse<Area>> {
        val taskCompletionSource = TaskCompletionSource<GenericResponse<Area>>()
        fireStore.runTransaction {
            // Create area
            val fbArea = area.toFBArea()

            // Update areas
            val newAreaRef = areasReference().document()
            fbArea.id = newAreaRef.id
            it.set(newAreaRef, fbArea)
            Log.d(TAG, "New Area id: " + newAreaRef.id + " for user: " + userUid)

            // Update user
            val userRef = usersReference().document(userUid);
            val fbUserArea = fbArea.toFBUserArea()
            userRef.collection(AREAS).document(newAreaRef.id).set(fbUserArea)
        }.addOnSuccessListener {
            Log.d(TAG, "User area added")
            taskCompletionSource.setResult(GenericResponse(success = true, result = area))
        }.addOnFailureListener {
            Log.d(TAG, "User area exception")
            taskCompletionSource.setException(it)
        }
        return taskCompletionSource.task
    }

    fun putAreaPlant(userUid: String, areaId: String, plant: Plant): Task<GenericResponse<Plant>> {
        val taskCompletionSource = TaskCompletionSource<GenericResponse<Plant>>()
        fireStore.runTransaction {
            // Create plant
            val fbPlant = plant.toFBPlant()
            // Update plants with current present id or create a new one
            val newPlantRef = if (!TextUtils.isEmpty(plant.id)) plantsReference().document(plant.id!!) else plantsReference().document()
            val plantId = newPlantRef.id
            // Update references
            plant.id = plantId
            fbPlant.id = plantId
            it.set(newPlantRef, fbPlant)
            Log.d(TAG, "New Plant id: " + newPlantRef.id + " for user: " + userUid + " in area: " + areaId)
            // Add plant to area
            val areaPlant = plant.toFBAreaPlant()
            val areaPlantMap = mapOf(plantId to areaPlant)
            val areaPlantsRef = areasReference().document(areaId)
            areaPlantsRef.update("$PLANTS.$plantId", areaPlant)
            // Add plant to user area
            val userAreaPlantsRef = usersReference().document(userUid).collection(AREAS).document(areaId)
            userAreaPlantsRef.update("$PLANTS.$plantId", areaPlant)
        }.addOnSuccessListener {
            Log.d(TAG, "Plant added")
            taskCompletionSource.setResult(GenericResponse(success = true, result = plant))
        }.addOnFailureListener {
            Log.d(TAG, "Plant exception")
            taskCompletionSource.setException(it)
        }
        return taskCompletionSource.task
    }

    fun toggleActiveToPlant(userId: String, areaId: String, plant: Plant, active: Boolean): Task<GenericResponse<Boolean>> {
        val taskCompletionSource = TaskCompletionSource<GenericResponse<Boolean>>()
        fireStore.runTransaction {
            val plantRef = plantsReference().document(plant.id!!)
            plantRef.update("active", active)
            // Update field in area
            val refInArea = "plants.${plant.id}.active"
            Log.d(TAG, "TEST: $refInArea")
            val areaPlantsRef = areasReference().document(areaId)
            areaPlantsRef.update(refInArea,active)
            // Update field in user area
            val userAreaPlantsRef = usersReference().document(userId).collection(AREAS).document(areaId)
            userAreaPlantsRef.update(refInArea,active)
        }.addOnSuccessListener {
            Log.d(TAG, "Plant active status changed to $active")
            taskCompletionSource.setResult(GenericResponse(success = true, result = active))
        }.addOnFailureListener {
            Log.d(TAG, "Plant active status exception")
            taskCompletionSource.setException(it)
        }
        return taskCompletionSource.task
    }

    // Queries

    fun getUserAreasQuery(userUid: String): Query {
        // Add user info
        return usersReference().document(userUid).collection(AREAS)
    }

    fun getPlantSensorDataQuery(plantId: String, collection: String): Query {
        return plantsReference().document(plantId).collection(collection)
    }
}