package com.gardencaretaker.storage.model

data class FBSensorData(
    val value: Float = 0.0f,
    val timestamp: Long = 0L
)