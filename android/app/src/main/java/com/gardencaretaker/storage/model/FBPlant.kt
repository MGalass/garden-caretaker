package com.gardencaretaker.storage.model

import com.gardencaretaker.model.Plant

data class FBPlant(
    var id: String? = "",
    var user: String? = "",
    var area: String? = "",
    var active: Boolean? = false,
    var warning: Boolean? = false,
    var name: String? = "",
    var species: String? = "",
    var apiId: Int? = null,

    var recommendedBrigthness: String? = Plant.DEFAULT_BRIGHTNESS,
    var recommendedSoilHumidity: String? = Plant.DEFAULT_SOIL_HUMIDITY,
    var precipitationMinimum: Double? = null,
    var precipitationMaximum: Double? = null,
    var temperatureMinimum: Double? = null
) {
    fun toFBAreaPlant() = FBAreaPlant(
        id = id,
        active = active,
        warning = warning,
        name = name,
        species = species,
        apiId = apiId,
        recommendedBrigthness = recommendedBrigthness,
        recommendedSoilHumidity = recommendedSoilHumidity,
        precipitationMinimum = precipitationMinimum,
        precipitationMaximum = precipitationMaximum,
        temperatureMinimum = temperatureMinimum
    )
}

