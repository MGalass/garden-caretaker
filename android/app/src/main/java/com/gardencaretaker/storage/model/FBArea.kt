package com.gardencaretaker.storage.model

data class FBArea (
    var id: String? = "",
    var user: String? = "",
    var name: String? = "",
    var type: String? = "",
    var address: String? = "",
    var plants: Map<String, FBAreaPlant>? = HashMap()
    //var plants: List<FBAreaPlant>? = ArrayList()
) {
    fun toFBUserArea() = FBUserArea(
        id,
        name,
        type,
        plants
    )
}

