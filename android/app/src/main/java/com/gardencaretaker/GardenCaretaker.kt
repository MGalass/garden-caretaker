package com.gardencaretaker

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.gardencaretaker.storage.UserPreferences
import com.gardencaretaker.ui.main.view.MainActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging

class GardenCaretaker: Application() {

    companion object {
        const val TAG = "GardenCaretaker"

        const val NOTIFICATION_DEFAULT_CHANNEL_ID = "default-channel"
        const val NOTIFICATION_USERS = "user-%s"
        const val NOTIFICATION_DEFAULT_ID = 1

        lateinit var instance: GardenCaretaker
            private set

        fun getResourceString(resId: Int): String {
            return instance.applicationContext.getString(resId)
        }

        fun getDrawable(drawableRes: Int): Drawable {
            return instance.getDrawable(drawableRes)!!
        }

    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        // Create notification channel (if required)
        createNotificationChannel()

        // Retrieving Firebase token
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                Log.d(TAG, "Retrieved token: $token")
            })

        if (UserPreferences.getUserId().isNotEmpty()) {
            // Register to topic
            registerToUserNotificationTopic(UserPreferences.getUserId())
        }
    }

    private fun registerToNotificationTopic(topic: String) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic)
            .addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "User subscribed to topic [$topic] failed: ", task.exception)
                    return@addOnCompleteListener
                }
                Log.d(TAG, "User successfully subscribed to topic [$topic]")
            }
    }

    fun registerToUserNotificationTopic(userId: String) {
        registerToNotificationTopic(String.format(NOTIFICATION_USERS, userId))
    }

    private fun createNotificationChannel() {
        // Create a notification channel for Oreo+ devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_default)
            val descriptionText = getString(R.string.channel_default_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(NOTIFICATION_DEFAULT_CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun fireWarningNotification(title: String, body: String) {
        // Create an explicit intent for an Activity in your app
        val intent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent =  PendingIntent.getActivity(this, 0, intent, 0)

        val notification = NotificationCompat.Builder(applicationContext, NOTIFICATION_DEFAULT_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_flower_outline)
            .setColor(ContextCompat.getColor(this, R.color.primaryColor))
            .setContentTitle(title)
            .setContentText(body)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()

        with(NotificationManagerCompat.from(this)) {
            notify(NOTIFICATION_DEFAULT_ID, notification)
        }
    }

}

fun Context.gardenCaretaker() = GardenCaretaker.instance