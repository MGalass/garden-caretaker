package com.gardencaretaker.model

import com.gardencaretaker.storage.model.FBAreaPlant
import com.gardencaretaker.storage.model.FBPlant
import com.gardencaretaker.storage.model.FBUser

data class User(
    var userId: String? = "",
    var username: String? = "",
    var areas: List<Area>? = ArrayList()
) {
    companion object {
        fun fromFBUser(fbUSer: FBUser): User {
            return User(
                userId = fbUSer.userId,
                username = fbUSer.username,
                areas = fbUSer.areas?.map { Area.fromFBUserArea(it) }
            )
        }
    }

    fun toFBUser() = FBUser(
        userId,
        username,
        areas?.map { it.toFBArea().toFBUserArea() }
    )
}