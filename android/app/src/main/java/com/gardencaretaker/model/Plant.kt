package com.gardencaretaker.model

import com.gardencaretaker.storage.model.FBAreaPlant
import com.gardencaretaker.storage.model.FBPlant
import java.util.*

data class Plant(
    var id: String? = "",
    var user: String? = "",
    var area: String? = "",
    var active: Boolean? = false,
    var warning: Boolean? = false,
    var name: String? = "",
    var species: String? = "",
    var apiId: Int? = null,

    var recommendedBrigthness: String? = DEFAULT_BRIGHTNESS,
    var recommendedSoilHumidity: String? = DEFAULT_SOIL_HUMIDITY,
    var precipitationMinimum: Double? = null,
    var precipitationMaximum: Double? = null,
    var temperatureMinimum: Double? = null
) {

    companion object {
        const val LOW_VALUE = "Low"
        const val MEDIUM_VALUE = "Medium"
        const val HIGH_VALUE = "High"
        const val DEFAULT_SOIL_HUMIDITY = MEDIUM_VALUE
        const val DEFAULT_BRIGHTNESS = MEDIUM_VALUE
        const val BRIGHTNESS_LOW_VALUE = 400
        const val BRIGHTNESS_HIGH_VALUE = 900
        const val SOIL_HUMIDITY_WATER_VALUE = 355 // In water
        const val SOIL_HUMIDITY_AIR_VALUE = 734
        const val SOIL_HUMIDITY_DELTA = 100

        fun fromFBPlant(fbPlant: FBPlant): Plant {
            return Plant(
                id = fbPlant.id,
                user = fbPlant.user,
                area = fbPlant.area,
                warning = fbPlant.warning,
                active = fbPlant.active,
                name = fbPlant.name,
                species = fbPlant.species,
                apiId = fbPlant.apiId,
                recommendedBrigthness = fbPlant.recommendedBrigthness,
                recommendedSoilHumidity = fbPlant.recommendedSoilHumidity,
                precipitationMinimum = fbPlant.precipitationMinimum,
                precipitationMaximum = fbPlant.precipitationMaximum,
                temperatureMinimum = fbPlant.temperatureMinimum
            )
        }

        fun fromFBAreaPlant(fbAreaPlant: FBAreaPlant): Plant {
            return Plant(
                id = fbAreaPlant.id,
                active = fbAreaPlant.active,
                warning = fbAreaPlant.warning,
                name = fbAreaPlant.name,
                species = fbAreaPlant.species,
                apiId = fbAreaPlant.apiId,
                recommendedBrigthness = fbAreaPlant.recommendedBrigthness,
                recommendedSoilHumidity = fbAreaPlant.recommendedSoilHumidity,
                precipitationMinimum = fbAreaPlant.precipitationMinimum,
                precipitationMaximum = fbAreaPlant.precipitationMaximum,
                temperatureMinimum = fbAreaPlant.temperatureMinimum
            )
        }
    }

    private fun capitalizeSpecies(): String {
        return species?.substring(0, 1)?.toUpperCase(Locale.getDefault()) + species?.substring(1)
    }

    fun toFBPlant() = FBPlant(
        id = id,
        user = user,
        area = area,
        active = active,
        warning = warning,
        name = name,
        species = capitalizeSpecies(),
        apiId = apiId,
        recommendedBrigthness = recommendedBrigthness,
        recommendedSoilHumidity = recommendedSoilHumidity,
        precipitationMinimum = precipitationMinimum,
        precipitationMaximum = precipitationMaximum,
        temperatureMinimum = temperatureMinimum
    )

    fun toFBAreaPlant() = FBAreaPlant(
        id = id,
        active = active,
        warning = warning,
        name = name,
        species = capitalizeSpecies(),
        apiId = apiId,
        recommendedBrigthness = recommendedBrigthness,
        recommendedSoilHumidity = recommendedSoilHumidity,
        precipitationMinimum = precipitationMinimum,
        precipitationMaximum = precipitationMaximum,
        temperatureMinimum = temperatureMinimum
    )
}

