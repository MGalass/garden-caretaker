package com.gardencaretaker.model

import com.gardencaretaker.storage.model.FBArea
import com.gardencaretaker.storage.model.FBUserArea

data class Area (
    var id: String? = "",
    var user: String? = "",
    var name: String? = "",
    var type: String? = "",
    var address: String? = "",
    var plants: List<Plant>? = ArrayList()
) {

    companion object {
        fun fromFBArea(fbArea: FBArea): Area {
            return Area(
                id = fbArea.id,
                user = fbArea.user,
                name = fbArea.name,
                type = fbArea.type,
                address = fbArea.address,
                plants = fbArea.plants?.map { Plant.fromFBAreaPlant(it.value) }
            )
        }

        fun fromFBUserArea(fbUserArea: FBUserArea): Area {
            return Area(
                id = fbUserArea.id,
                name = fbUserArea.name,
                type = fbUserArea.type,
                plants = fbUserArea.plants?.map { Plant.fromFBAreaPlant(it.value) }
            )
        }
    }

    fun toFBArea() = FBArea(
        id,
        user,
        name,
        type,
        address,
        plants?.map { it.toFBPlant().toFBAreaPlant() }?.associateBy {it.id!!}
        //plants?.map { it.toFBPlant().toFBAreaPlant() }?.map { mapOf(it.id to it) }
    )
}

