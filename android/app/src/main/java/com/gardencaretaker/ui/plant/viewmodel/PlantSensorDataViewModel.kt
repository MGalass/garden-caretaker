package com.gardencaretaker.ui.plant.viewmodel

import androidx.lifecycle.ViewModel

class PlantSensorDataViewModel(
    val plantId: String?,
    val plantApiId: Int?,
    val plantSpeciesName: String?
) : ViewModel() {

    private val TAG = "PlantSensorDataVM"

    init { }

}