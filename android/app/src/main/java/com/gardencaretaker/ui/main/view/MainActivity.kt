package com.gardencaretaker.ui.main.view

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ActivityMainBinding
import com.gardencaretaker.factory.ViewModelFactory
import com.gardencaretaker.gardenCaretaker
import com.gardencaretaker.model.Plant
import com.gardencaretaker.ui.area.view.CreateAreaActivity
import com.gardencaretaker.ui.base.view.BaseActivity
import com.gardencaretaker.ui.main.adapter.AreasAdapter
import com.gardencaretaker.ui.main.viewmodel.MainViewModel
import com.gardencaretaker.ui.plant.view.PlantSensorDataActivity
import com.gardencaretaker.ui.plantpairing.view.PlantPairingActivity
import com.gardencaretaker.ui.search.view.SearchPlantActivity

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: AreasAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)

        viewModel = ViewModelProvider(this, ViewModelFactory(MainViewModel())).get(MainViewModel::class.java)
        binding.viewModel = this.viewModel

        adapter = AreasAdapter(this, object : AreasAdapter.AreasAdapterListener {
            override fun onAddPlantRequest(areaId: String) {
                // Open PlantPairingActivity
                val intent = Intent(this@MainActivity, PlantPairingActivity::class.java)
                intent.putExtra(PlantPairingActivity.KEY_AREA_ID, areaId)
                startActivity(intent)
            }

            override fun onViewPlantSensorDataRequest(plant: Plant) {
                // Open PlantSensorDataActivity
                val intent = Intent(this@MainActivity, PlantSensorDataActivity::class.java)
                intent.putExtra(PlantSensorDataActivity.KEY_PLANT_ID, plant.id)
                intent.putExtra(PlantSensorDataActivity.KEY_PLANT_API_ID, plant.apiId)
                intent.putExtra(PlantSensorDataActivity.KEY_PLANT_SPECIES_NAME, plant.name)
                startActivity(intent)
            }

            override fun onDataChange(itemsCount: Int) {
                // Show empty data ui message
                viewModel.getNoAreas().postValue(itemsCount == 0)
            }
        })
        binding.mainContent.mainRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.mainContent.mainRecyclerView.adapter = adapter

        binding.mainAddArea.setOnClickListener {
            startActivity(Intent(this, CreateAreaActivity::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }

}
