package com.gardencaretaker.ui.base.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.gardencaretaker.GardenCaretaker
import com.gardencaretaker.gardenCaretaker
import com.gardencaretaker.storage.UserPreferences
import com.gardencaretaker.ui.main.view.MainActivity
import com.gardencaretaker.ui.splash.view.SplashScreenActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

open class BaseActivity : AppCompatActivity() {

    private val TAG = "BaseA"

    private lateinit var auth: FirebaseAuth
    private var user: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Retrieve FirebaseAuth instance to check if we are already logged in
        this.auth = FirebaseAuth.getInstance()
        this.user = this.auth.currentUser

        if (this.user == null) {
            // User has been logged out
            Log.d(TAG, "User has logged out")
            UserPreferences.clear()
            if (this !is SplashScreenActivity) {
                val intent = Intent(this, SplashScreenActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        } else {
            // User is already logged in
            Log.d(TAG, "User is already logged in")

            // Subscribe to notification topic
            val userId = UserPreferences.getUserId()
            if (userId.isNotEmpty()) {
                gardenCaretaker().registerToUserNotificationTopic(userId)
            }

            if (this is SplashScreenActivity) {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
