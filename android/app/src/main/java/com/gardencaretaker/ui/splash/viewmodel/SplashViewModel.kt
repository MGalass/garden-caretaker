package com.gardencaretaker.ui.splash.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gardencaretaker.GardenCaretaker
import com.gardencaretaker.model.User
import com.gardencaretaker.storage.FirebaseDatabaseManager
import com.gardencaretaker.storage.UserPreferences
import com.gardencaretaker.utils.GenericResponse
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*

class SplashViewModel : ViewModel() {

    private val TAG = "LoginVM"

    private val username: MutableLiveData<String> = MutableLiveData()
    private val password: MutableLiveData<String> = MutableLiveData()
    private val loginMode: MutableLiveData<Boolean> = MutableLiveData()
    private val loading: MutableLiveData<Boolean> = MutableLiveData()

    private val authAttemptObservable: MutableLiveData<GenericResponse<Boolean>> = MutableLiveData()

    init {
        loginMode.value = true
        username.value = UserPreferences.getUsername()
    }

    fun getUsername() = username
    fun getPassword() = password
    fun getLoginMode() = loginMode
    fun isLoading() = loading
    fun getAuthAttemptObservable() = authAttemptObservable

    fun toggleMode(isLogin: Boolean) {
        loginMode.value = isLogin
        Log.d(TAG, "Mode is now login: " + loginMode.value)
    }

    fun attemptAuth() {
        val onSuccessListener: OnSuccessListener<AuthResult> = OnSuccessListener {
            // Login/Sign successful
            loading.postValue(false)
            val user = it.user
            Log.d(TAG, "Auth success")
            Log.d(TAG, user!!.uid)

            // Save the user credentials
            UserPreferences.putUserId(user!!.uid)
            UserPreferences.putUsername(username.value!!)
            // Subscribe to topic
            GardenCaretaker.instance.registerToUserNotificationTopic(user!!.uid)

            if (!loginMode.value!!) {
                // During signup, we must add the user to the database
                saveUser(User(userId = user.uid, username = username.value!!))
                    .addOnSuccessListener {
                        // User added successfully
                        authAttemptObservable.postValue(GenericResponse(success = true))
                    }
                    .addOnFailureListener {
                        // Add user failed
                        authAttemptObservable.postValue(GenericResponse(success = false, message = getFirebaseErrorMessage(it)))
                    }
            } else {
                authAttemptObservable.postValue(GenericResponse(success = true))
            }
        }

        val onFailureListener = OnFailureListener {
            // Login/Sign in failed
            loading.postValue(false)
            Log.d(TAG, "Auth failed: " + it.message)
            authAttemptObservable.postValue(GenericResponse(success = false, message = getFirebaseErrorMessage(it)))
        }

        if (!username.value.isNullOrEmpty() && !password.value.isNullOrEmpty()) {
            val username = username.value!!
            val password = password.value!!

            loading.postValue(true)

            // Login or sign up
            val auth = FirebaseAuth.getInstance()
            val task = if (loginMode.value!!) auth.signInWithEmailAndPassword(username, password) else auth.createUserWithEmailAndPassword(username, password)
            task.addOnSuccessListener(onSuccessListener).addOnFailureListener(onFailureListener)
        }
    }

    private fun saveUser(user: User): Task<Void> {
        return FirebaseDatabaseManager.putUser(user);
    }

    private fun getFirebaseErrorMessage(exception: Exception) = when(exception) {
        is FirebaseAuthWeakPasswordException -> "Password must be at least 6 characters"
        is FirebaseAuthUserCollisionException -> "The email address is already in use by another account"
        is FirebaseAuthInvalidCredentialsException -> "The email address is badly formatted"
        is FirebaseAuthInvalidUserException -> "The user has been deleted or is invalid"
        else -> "Error during authentication procedure"
    }

}