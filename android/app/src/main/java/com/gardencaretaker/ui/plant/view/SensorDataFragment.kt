package com.gardencaretaker.ui.plant.view

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.util.Predicate
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.gardencaretaker.GardenCaretaker
import com.gardencaretaker.R
import com.gardencaretaker.databinding.FragmentSensorDataBinding
import com.gardencaretaker.model.Plant
import com.gardencaretaker.model.Plant.Companion.BRIGHTNESS_HIGH_VALUE
import com.gardencaretaker.model.Plant.Companion.BRIGHTNESS_LOW_VALUE
import com.gardencaretaker.model.Plant.Companion.SOIL_HUMIDITY_AIR_VALUE
import com.gardencaretaker.model.Plant.Companion.SOIL_HUMIDITY_DELTA
import com.gardencaretaker.model.Plant.Companion.SOIL_HUMIDITY_WATER_VALUE
import com.gardencaretaker.storage.FirebaseDatabaseManager
import com.gardencaretaker.storage.model.FBSensorData
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.QuerySnapshot
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SensorDataFragment(
    val position: Int,
    val plantData: Plant,
    val sensorDataType: SensorDataType
): Fragment() {

    private val TAG = "SensorDataFragment"

    private var entryCounter = 0
    private val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())

    enum class SensorDataType(val type: Int, val sensorDataKey: String, val sensorDataName: String, val checkDataErrorPredicate: Predicate<Float>) {
        BRIGHTNESS(0, "brightness", GardenCaretaker.getResourceString(R.string.brightness), Predicate { true }),
        TEMPERATURE(1, "temperature", GardenCaretaker.getResourceString(R.string.temperature), Predicate { true }),
        SOIL_HUMIDITY(2, "soilHumidity", GardenCaretaker.getResourceString(R.string.soil_humidity), Predicate { true }),
        RAINFALL(3, "rainfall", GardenCaretaker.getResourceString(R.string.rainfall), Predicate { true });

        companion object {
            fun fromType(type: Int): SensorDataType {
                return values().find { it.type == type }!!
            }
        }

    }

    private lateinit var binding: FragmentSensorDataBinding

    private val querySnapshotsListener = object : (QuerySnapshot?, FirebaseFirestoreException?) -> Unit {
        override fun invoke(querySnapshot: QuerySnapshot?, exception: FirebaseFirestoreException?) {
            if (exception != null) {
                Log.d(TAG, "Exception in snapshotsListener: ${exception.message}")
                return
            }

            if (querySnapshot != null) {
                querySnapshot.documentChanges.forEach {
                    when (it.type) {
                        DocumentChange.Type.ADDED -> {
                            Log.d(TAG, "New value: ${it.document.data}")
                            val sensorData = it.document.toObject(FBSensorData::class.java)
                            Log.d(TAG, "SensorData: ${sensorData}")
                            manageSensorData(sensorData)
                        }
                        DocumentChange.Type.MODIFIED -> Log.d(TAG, "Modified value: ${it.document.data}")
                        DocumentChange.Type.REMOVED -> Log.d(TAG, "Removed value: ${it.document.data}")
                    }
                }
            } else {
                Log.d(TAG, "Current data: null")
            }
        }
    }

    private var listenerRegistration : ListenerRegistration? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflating fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sensor_data, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    private fun setupChart(chart: LineChart) {
        // xAxis
        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.setEnabled(false)
        // yAxis
        chart.axisRight.setEnabled(false)
        val leftAxis = chart.axisLeft
        leftAxis.removeAllLimitLines()
        leftAxis.axisMaximum = 1024f
        leftAxis.axisMinimum = 0f

        chart.setDrawGridBackground(false)
        chart.setTouchEnabled(true)
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        val limitLines = ArrayList<LimitLine>()

        when (sensorDataType) {
            SensorDataType.RAINFALL -> {
                limitLines.add(LimitLine(550f, "No rain"))
                limitLines.add(LimitLine(450f, "Heavy rain"))
            }
            SensorDataType.SOIL_HUMIDITY -> {
                limitLines.add(LimitLine(SOIL_HUMIDITY_AIR_VALUE.toFloat(), "Open air - minimum humidity"))
                limitLines.add(LimitLine(SOIL_HUMIDITY_WATER_VALUE.toFloat(), "In water - maximum humidity"))
            }
            SensorDataType.BRIGHTNESS -> {
                limitLines.add(LimitLine(BRIGHTNESS_HIGH_VALUE.toFloat(), "High brightness"))
                limitLines.add(LimitLine((BRIGHTNESS_HIGH_VALUE.toFloat() + BRIGHTNESS_LOW_VALUE.toFloat()) / 2, "Medium brightness"))
                limitLines.add(LimitLine(BRIGHTNESS_LOW_VALUE.toFloat(), "Low brightness"))
            }
            SensorDataType.TEMPERATURE -> {
                plantData.temperatureMinimum?.let {
                    val minTemperatureLine = LimitLine(it.toFloat(), "Minimum temperature for this plant")
                    minTemperatureLine.lineColor = Color.MAGENTA
                    limitLines.add(minTemperatureLine)

                    leftAxis.axisMaximum = 40f
                    leftAxis.axisMinimum = -40f
                }
            }
        }

        limitLines.forEach {
            leftAxis.addLimitLine(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "Fragment ${sensorDataType.sensorDataName}: onViewCreated - plantId: ${plantData.id}")
        binding.fragmentSensorDataText.text = sensorDataType.sensorDataName

        val chart = binding.fragmentSensorDataLineChart

        // Setup basic chart configuration based on the current fragment
        setupChart(chart)

        // Set custom marker view
        chart.marker = SensorDataMarkerView(context, R.layout.item_data_entry)

        // Set an empty dataSet
        val lineDataSet = LineDataSet(ArrayList(), sensorDataType.sensorDataName)
        lineDataSet.setDrawValues(false)
        chart.data = LineData(lineDataSet)
        chart.description = Description().apply { text = "" }
    }

    private fun isPlantInDangerForBrightness(recommendedBrightness: String?, brightness: Float): Boolean {
        recommendedBrightness?.let {
            when (it) {
                Plant.MEDIUM_VALUE -> return brightness < BRIGHTNESS_LOW_VALUE || brightness > BRIGHTNESS_HIGH_VALUE
                Plant.HIGH_VALUE -> return brightness < BRIGHTNESS_HIGH_VALUE
                else -> brightness > BRIGHTNESS_LOW_VALUE
            }
        }
        return false
    }

    private fun isPlantInDangerForTemperature(temperatureMinimum: Double?, temperature: Float): Boolean {
        temperatureMinimum?.let {
            return temperature < it
        }
        return false
    }

    private fun isPlantInDangerForSoilHumidity(recommendedSoilHumidity: String?, soilHumidity: Float): Boolean {
        recommendedSoilHumidity?.let {
            when (it) {
                Plant.MEDIUM_VALUE -> return soilHumidity > SOIL_HUMIDITY_AIR_VALUE - SOIL_HUMIDITY_DELTA || soilHumidity < SOIL_HUMIDITY_WATER_VALUE + SOIL_HUMIDITY_DELTA
                Plant.HIGH_VALUE -> return soilHumidity > SOIL_HUMIDITY_WATER_VALUE + SOIL_HUMIDITY_DELTA
                else -> soilHumidity < SOIL_HUMIDITY_AIR_VALUE - SOIL_HUMIDITY_DELTA
            }
        }
        return false
    }

    private fun isPlantInDangerForRainfall(precipitationMinimum: Double?, precipitationMaximum: Double?, rainfall: Float): Boolean {
        if (precipitationMinimum == null && precipitationMaximum == null) return false
        precipitationMinimum?.let { min ->
            precipitationMaximum?.let { max -> return rainfall < min || rainfall > max } // Both values
            // No maximum
            return rainfall < min
        }
        precipitationMaximum?.let { max -> return rainfall > max } // No minimum
        return false
    }

    private fun isPlantInDanger(sensorData: Float?): Boolean {
        sensorData?.let {
            when (sensorDataType) {
                SensorDataType.BRIGHTNESS -> return isPlantInDangerForBrightness(plantData.recommendedBrigthness, it)
                SensorDataType.TEMPERATURE -> return isPlantInDangerForTemperature(plantData.temperatureMinimum, it)
                SensorDataType.SOIL_HUMIDITY -> return isPlantInDangerForSoilHumidity(plantData.recommendedSoilHumidity, it)
                SensorDataType.RAINFALL -> return isPlantInDangerForRainfall(plantData.precipitationMinimum, plantData.precipitationMaximum, it) && false
            }
        }
        return false
    }

    fun manageSensorData(sensorData: FBSensorData) {
        entryCounter += 1

        // Formatting timestamp
        val formattedDate = sdf.format(Date(sensorData.timestamp * 1000L))

        // Creating entry
        val sensorDataMarkerInfo = SensorDataMarkerView.SensorDataMarkerInfo(formattedDate, sensorData.value)
        // Check if the plant is in danger based on the current value
        var entry = Entry(entryCounter.toFloat(), sensorData.value, sensorDataMarkerInfo)
        if (isPlantInDanger(sensorData.value)) {
            val errorDrawable = context!!.getDrawable(R.drawable.ic_warning_black_24dp)
            context?.let {
                DrawableCompat.setTint(errorDrawable!!, ContextCompat.getColor(it, R.color.primaryColor))
            }
            entry = Entry(entryCounter.toFloat(), sensorData.value, errorDrawable, sensorDataMarkerInfo)
        }

        // Updating last measurement
        binding.lastMeasurementDate = formattedDate
        binding.lastMeasurementValue = sensorData.value.toString()

        binding.fragmentSensorDataLineChart.data.getDataSetByIndex(0).addEntry(entry)
        binding.fragmentSensorDataLineChart.data.notifyDataChanged()
        binding.fragmentSensorDataLineChart.notifyDataSetChanged()
        binding.fragmentSensorDataLineChart.invalidate()
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "Fragment ${sensorDataType.sensorDataName}: onResume - ADDING LISTENER on ${sensorDataType.sensorDataKey}")

        // Setting the listener for the sensor data
        listenerRegistration = FirebaseDatabaseManager.getPlantSensorDataQuery(plantData.id!!, sensorDataType.sensorDataKey).addSnapshotListener(querySnapshotsListener)
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "Fragment ${sensorDataType.sensorDataName}: onPause - REMOVING LISTENER")

        // Removing the listener
        listenerRegistration?.remove()
        binding.fragmentSensorDataLineChart.data.getDataSetByIndex(0).clear()
        entryCounter = 0
    }

}