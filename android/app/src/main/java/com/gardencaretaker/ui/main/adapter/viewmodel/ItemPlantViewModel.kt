package com.gardencaretaker.ui.main.adapter.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.gardencaretaker.model.Plant
import com.gardencaretaker.storage.FirebaseDatabaseManager
import com.gardencaretaker.storage.UserPreferences

class ItemPlantViewModel(val plant: Plant, val areaId: String, private val listener: ItemPlantViewModelListener?) {

    interface ItemPlantViewModelListener {
        fun onViewPlantSensorDataEvent(plant: Plant)
    }

    private val TAG = "ItemPlantVM"

    private val active = MutableLiveData<Boolean>()

    init {
        active.value = plant.active
    }

    fun getName() = plant.name
    fun getActive() = active.value
    fun getWarning() = plant.warning

    fun toggleActive() {
        FirebaseDatabaseManager.toggleActiveToPlant(
                UserPreferences.getUserId(),
                areaId,
                plant = plant, active = !active.value!!)
            .addOnSuccessListener {
                // Status has changed
                active.postValue(!active.value!!)
            }.addOnFailureListener {
                // Status has not changed
                Log.d(TAG, "Exception: ${it.message}")
            }
    }

    fun goToSensorData() {
        // Propagating event on listener
        listener?.onViewPlantSensorDataEvent(plant)
    }
}