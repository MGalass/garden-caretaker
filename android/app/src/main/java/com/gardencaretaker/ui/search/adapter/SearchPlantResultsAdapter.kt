package com.gardencaretaker.ui.search.adapter.viewmodel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import androidx.recyclerview.widget.RecyclerView
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ItemSearchPlantResultBinding
import com.gardencaretaker.network.model.TreflePlantSearchResult

class SearchPlantResultsAdapter(val lifecycleOwner: LifecycleOwner?, val listener: SearchPlantResultsAdapterListener?)
    : PagedListAdapter<TreflePlantSearchResult, SearchPlantResultsAdapter.SearchPlantResultViewHolder>(DIFF_CALLBACK) {

    companion object {
        // Callback to let the adapter know when two results are really different
        val DIFF_CALLBACK : ItemCallback<TreflePlantSearchResult> = object : ItemCallback<TreflePlantSearchResult>() {
            override fun areItemsTheSame(oldItem: TreflePlantSearchResult, newItem: TreflePlantSearchResult): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: TreflePlantSearchResult, newItem: TreflePlantSearchResult): Boolean {
                return oldItem.equals(newItem)
            }

        }
    }

    interface SearchPlantResultsAdapterListener {
        fun onSearchPlantResultSelected(plantApiId: Int)
    }

    private val itemAreaViewModelListener = object: ItemSearchPlantResultViewModel.ItemSearchPlantResultViewModelListener {
        override fun onSelectSearchPlantResult(plantApiId: Int) {
            // The user selected a search result
            listener?.onSearchPlantResultSelected(plantApiId)
        }
    }

    init { }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchPlantResultViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_plant_result, parent, false)
        return SearchPlantResultViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchPlantResultViewHolder, position: Int) {
        if (position < itemCount) {
            val plant = getItem(position)
            plant?.let {
                holder.getBinding().viewModel = ItemSearchPlantResultViewModel(it, itemAreaViewModelListener)
                holder.getBinding().lifecycleOwner = lifecycleOwner
                holder.getBinding().executePendingBindings()
            }
        }
    }

    class SearchPlantResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemSearchPlantResultBinding
        init {
            binding = DataBindingUtil.bind(itemView)!!
        }

        fun getBinding() = binding
    }
}