package com.gardencaretaker.ui.area.viewmodel

import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gardencaretaker.GardenCaretaker
import com.gardencaretaker.R
import com.gardencaretaker.model.Area
import com.gardencaretaker.model.Plant
import com.gardencaretaker.storage.FirebaseDatabaseManager
import com.gardencaretaker.storage.UserPreferences
import com.gardencaretaker.utils.GenericResponse

class CreateAreaViewModel: ViewModel() {

    private val TAG = "CreateAreaVM"

    private val areaName = MutableLiveData<String>()
    private val areaType = MutableLiveData<String>()
    private val areaTypeExplanation = MutableLiveData<String>()
    private val areaAddress = MutableLiveData<String>()

    private val saveAttemptObservable = MutableLiveData<GenericResponse<Boolean>>()

    init {
        areaName.value = ""
        areaAddress.value = ""
    }

    fun getAreaName() = areaName;
    fun getAreaType() = areaType;
    fun getAreaAddress() = areaAddress;
    fun getAreaTypeExplanation() = areaTypeExplanation

    fun getSaveAttemptObservable() = saveAttemptObservable

    fun saveArea() {
        // Saving area
        if (TextUtils.isEmpty(areaName.value!!) || TextUtils.isEmpty(areaAddress.value!!)) {
            saveAttemptObservable.postValue(GenericResponse(success = false, message = GardenCaretaker.getResourceString(R.string.error_incorrect_fields)))
            return
        }

        val userId = UserPreferences.getUserId()
        val area = Area(
            user = userId,
            name = areaName.value,
            address = areaAddress.value,
            type = areaType.value!!,
            plants = ArrayList()
        )

        FirebaseDatabaseManager.putUserArea(userId, area).addOnCompleteListener {
            if (it.isSuccessful) {
                // Area.kt saved
                saveAttemptObservable.postValue(GenericResponse(success = true))
            } else {
                // Area.kt was not saved due to an error
                Log.d(TAG, it.exception!!.message!!)
                saveAttemptObservable.postValue(GenericResponse(success = false, message = it.exception!!.message!!))
            }
        }
    }
}