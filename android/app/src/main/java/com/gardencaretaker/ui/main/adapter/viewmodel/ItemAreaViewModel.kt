package com.gardencaretaker.ui.main.adapter.viewmodel

import androidx.lifecycle.MutableLiveData
import com.gardencaretaker.GardenCaretaker
import com.gardencaretaker.R
import com.gardencaretaker.model.Area
import java.util.*

class ItemAreaViewModel(private val area: Area, private val listener: ItemAreaViewModelListener?) {

    interface ItemAreaViewModelListener {
        fun onAddPlantToAreaEvent(areaId: String)
    }

    private val showPlantsInfo: MutableLiveData<Boolean> = MutableLiveData()

    init {
        showPlantsInfo.value = true
    }

    fun getName() = area.name
    fun getType() = area.type
    fun getWarningPlantsNumber() = area.plants?.filter { it.warning!! }?.size

    fun showPlantsSection() = area.plants != null && area.plants!!.isNotEmpty()
    fun getTotalPlants() = String.format(Locale.getDefault(), GardenCaretaker.getResourceString(R.string.total_plants), area.plants?.size)
    fun getActivePlants() = String.format(Locale.getDefault(), GardenCaretaker.getResourceString(R.string.active_plants), area.plants?.filter { it.active!! }?.size )
    fun getWarningPlants() = String.format(Locale.getDefault(), GardenCaretaker.getResourceString(R.string.warning_plants), getWarningPlantsNumber())

    fun showPlantsInfo() = showPlantsInfo

    fun togglePlants() {
        showPlantsInfo.postValue(!showPlantsInfo.value!!)
    }

    fun addPlantToArea() {
        // Propagating event on listener
        listener?.onAddPlantToAreaEvent(area.id!!)
    }
}