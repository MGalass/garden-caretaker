package com.gardencaretaker.ui.plant.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gardencaretaker.model.Plant
import com.gardencaretaker.ui.plant.view.SensorDataFragment

class SensorDataViewPagerAdapter(fragmentActivity: FragmentActivity, val plantData: Plant) :
    FragmentStateAdapter(fragmentActivity) {

    companion object {
        const val TABS_COUNT = 4
    }

    override fun getItemCount(): Int {
        return TABS_COUNT
    }

    override fun createFragment(position: Int): Fragment {
        return SensorDataFragment(position = position, plantData = plantData, sensorDataType = SensorDataFragment.SensorDataType.fromType(position))
    }
}