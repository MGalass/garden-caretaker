package com.gardencaretaker.ui.plant.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gardencaretaker.GardenCaretaker
import com.gardencaretaker.R
import com.gardencaretaker.model.Plant
import com.gardencaretaker.network.model.TreflePlant
import com.gardencaretaker.network.services.TrefleService
import com.gardencaretaker.storage.FirebaseDatabaseManager
import com.gardencaretaker.storage.UserPreferences
import com.gardencaretaker.utils.GenericResponse
import kotlin.math.round

class PlantDetailViewModel(private val plantApiId: Int, private val plantId: String?, private val areaId: String?, private val showOnlyMode: Boolean) : ViewModel() {

    private val TAG = "PlantDetailVM"

    private var plantInfo = MutableLiveData<TreflePlant?>()
    private var isLoading = MutableLiveData<Boolean>()
    private var isFailed = MutableLiveData<Boolean>()

    private var plantAddedObservable = MutableLiveData<GenericResponse<Boolean>>()

    init {
        plantInfo.value = null
        isLoading.value = true
        isFailed.value = false

        // Fetching results from the API
        searchPlantInfo()
    }

    fun getPlantInfo() = plantInfo
    fun isLoading() = isLoading
    fun isFailed() = isFailed
    fun isShowOnlyMode() = showOnlyMode

    fun getPlantAddedObservable() = plantAddedObservable

    fun searchPlantInfo() {
        TrefleService.searchPlant(plantApiId).addOnSuccessListener {
            // Plant info retrieved successfully
            plantInfo.postValue(it)

            isLoading.value = false
        }.addOnFailureListener {
            // Could not retrieve plant info

            isFailed.value = true
            isLoading.value = false
        }
    }

    fun addPlantToArea() {
        if (!showOnlyMode) {
            // Add plant only if we are not in the show only mode
            plantInfo.value?.let {
                val userId = UserPreferences.getUserId()
                FirebaseDatabaseManager.putAreaPlant(
                    userId,
                    areaId!!,
                    Plant(
                        id = plantId,
                        user = userId,
                        area = areaId,
                        name = it.commonName,
                        species = it.mainSpecies?.scientificName,
                        apiId = plantApiId,                             // Saving the plant api id
                        recommendedSoilHumidity = it.mainSpecies?.growth?.moistureUse,
                        precipitationMinimum = it.mainSpecies?.growth?.precipitationMinimum?.cm?.apply { round(this) },
                        precipitationMaximum = it.mainSpecies?.growth?.precipitationMaximum?.cm?.apply { round(this) },
                        temperatureMinimum = it.mainSpecies?.growth?.temperatureMinimum?.degC?.apply { round(this) }
                    )
                ).addOnSuccessListener {
                    plantAddedObservable.postValue(GenericResponse(success = true, result = true))
                }.addOnFailureListener {
                    plantAddedObservable.postValue(GenericResponse(success = false, result = false, message = GardenCaretaker.getResourceString(R.string.error_add_plant)))
                }
            }
        }
    }

}