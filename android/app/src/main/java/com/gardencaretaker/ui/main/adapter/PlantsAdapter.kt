package com.gardencaretaker.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ItemPlantBinding
import com.gardencaretaker.model.Plant
import com.gardencaretaker.ui.main.adapter.viewmodel.ItemPlantViewModel

class PlantsAdapter(val lifecycleOwner: LifecycleOwner?, private val areaId: String, val listener: PlantsAdapterListener?): RecyclerView.Adapter<PlantsAdapter.PlantViewHolder>() {

    interface PlantsAdapterListener {
        fun onViewPlantSensorDataRequest(plant: Plant)
    }

    private val plants: ArrayList<Plant>

    private val itemPlantViewModelListener = object: ItemPlantViewModel.ItemPlantViewModelListener {
        override fun onViewPlantSensorDataEvent(plant: Plant) {
            // Propagating event
            listener?.onViewPlantSensorDataRequest(plant)
        }
    }

    init {
        plants = ArrayList()
    }

    fun addPlants(plants: List<Plant>) {
        this.plants.addAll(plants)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = plants.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlantViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_plant, parent, false)
        return PlantViewHolder(view)
    }

    override fun onBindViewHolder(holder: PlantViewHolder, position: Int) {
        if (position < itemCount) {
            val plant = plants.get(position)
            holder.getBinding().viewModel = ItemPlantViewModel(plant, areaId, itemPlantViewModelListener)
            holder.getBinding().lifecycleOwner = lifecycleOwner
            holder.getBinding().executePendingBindings()
        }    }

    class PlantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemPlantBinding
        init {
            binding = DataBindingUtil.bind(itemView)!!
        }

        fun getBinding() = binding
    }
}