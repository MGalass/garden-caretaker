package com.gardencaretaker.ui.plant.adapter.viewmodel

import com.gardencaretaker.network.model.PlantImage

class ItemPlantImageViewModel(val plantImage: PlantImage) {

    fun getUrl() = plantImage.url
}