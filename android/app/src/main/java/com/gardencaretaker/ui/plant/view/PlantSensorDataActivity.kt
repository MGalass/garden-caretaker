package com.gardencaretaker.ui.plant.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ActivityPlantSensorDataBinding
import com.gardencaretaker.factory.ViewModelFactory
import com.gardencaretaker.model.Plant
import com.gardencaretaker.storage.FirebaseDatabaseManager
import com.gardencaretaker.storage.model.FBPlant
import com.gardencaretaker.ui.base.view.BaseActivity
import com.gardencaretaker.ui.plant.adapter.SensorDataViewPagerAdapter
import com.gardencaretaker.ui.plant.viewmodel.PlantSensorDataViewModel
import com.google.android.material.tabs.TabLayoutMediator
import java.util.*

class PlantSensorDataActivity : BaseActivity() {

    companion object {
        private const val TAG = "PlantSensorDataA"
        const val KEY_PLANT_ID = "keyPlantId"
        const val KEY_PLANT_API_ID = "keyPlantApiId"
        const val KEY_PLANT_SPECIES_NAME = "keyPlantSpeciesName"
    }

    private lateinit var binding: ActivityPlantSensorDataBinding
    private lateinit var viewModel: PlantSensorDataViewModel

    @SuppressLint("DefaultLocale")
    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_plant_sensor_data)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val plantId = intent.extras?.getString(KEY_PLANT_ID, "")
        if (TextUtils.isEmpty(plantId)) {
            finish()
            return
        }

        val plantApiId = intent.extras?.getInt(KEY_PLANT_API_ID, 0)
        if (plantApiId == 0) {
            finish()
            return
        }

        val plantSpeciesName = intent.extras?.getString(KEY_PLANT_SPECIES_NAME, "")
        if (TextUtils.isEmpty(plantSpeciesName)) {
            finish()
            return
        }

        supportActionBar?.title = plantSpeciesName!!.capitalize(Locale.getDefault())

        viewModel = ViewModelProvider(this, ViewModelFactory(PlantSensorDataViewModel(plantId, plantApiId, plantSpeciesName))).get(PlantSensorDataViewModel::class.java)
        binding.viewModel = this.viewModel

    }

    override fun onStart() {
        super.onStart()
        FirebaseDatabaseManager.plantsReference().document(viewModel.plantId!!).get().addOnSuccessListener {
            val fbPlant = it.toObject(FBPlant::class.java)!!
            Log.d(TAG, fbPlant.toString())

            binding.viewPager.adapter = SensorDataViewPagerAdapter(this, Plant.fromFBPlant(fbPlant))
            TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
                tab.text = null
                tab.icon = getDrawable(when (position) {
                    0 -> R.drawable.ic_weather_sunny
                    1 -> R.drawable.ic_thermometer
                    2 -> R.drawable.ic_humidity
                    3 -> R.drawable.ic_weather_partly_rainy
                    else -> R.drawable.ic_weather_sunny
                })
            }.attach()

        }.addOnFailureListener {
            Log.d(TAG, "Error while retrieving plant data: ${it.message}")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_plant_sensor_data, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle item selection
        return when (item?.itemId) {
            R.id.menu_plant_details -> {
                // Open the PlantDetailActivity in show-only mode
                val intent = Intent(this@PlantSensorDataActivity, PlantDetailActivity::class.java)
                intent.putExtra(PlantDetailActivity.KEY_PLANT_ID, viewModel.plantId)
                intent.putExtra(PlantDetailActivity.KEY_PLANT_API_ID, viewModel.plantApiId)
                intent.putExtra(PlantDetailActivity.KEY_SHOW_ONLY_MODE, true)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
