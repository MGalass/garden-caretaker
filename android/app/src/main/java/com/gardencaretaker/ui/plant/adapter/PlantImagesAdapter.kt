package com.gardencaretaker.ui.plant.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ItemPlantImageBinding
import com.gardencaretaker.network.model.PlantImage
import com.gardencaretaker.ui.plant.adapter.viewmodel.ItemPlantImageViewModel

class PlantImagesAdapter(val lifecycleOwner: LifecycleOwner?): RecyclerView.Adapter<PlantImagesAdapter.PlantImageViewHolder>() {

    private val plantImages: ArrayList<PlantImage>

    init {
        plantImages = ArrayList()
    }

    fun addPlantImages(plantImages: List<PlantImage>) {
        this.plantImages.addAll(plantImages)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = plantImages.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlantImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_plant_image, parent, false)
        return PlantImageViewHolder(view)
    }

    override fun onBindViewHolder(holder: PlantImageViewHolder, position: Int) {
        if (position < itemCount) {
            val plantImage = plantImages.get(position)
            holder.getBinding().viewModel = ItemPlantImageViewModel(plantImage)
            holder.getBinding().lifecycleOwner = lifecycleOwner
            holder.getBinding().executePendingBindings()
        }    }

    class PlantImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemPlantImageBinding
        init {
            binding = DataBindingUtil.bind(itemView)!!
        }

        fun getBinding() = binding
    }
}