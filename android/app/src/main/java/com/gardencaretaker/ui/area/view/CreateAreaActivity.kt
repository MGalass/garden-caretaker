package com.gardencaretaker.ui.area.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ActivityCreateAreaBinding
import com.gardencaretaker.factory.ViewModelFactory
import com.gardencaretaker.ui.area.viewmodel.CreateAreaViewModel
import com.gardencaretaker.ui.base.view.BaseActivity
import com.google.android.material.snackbar.Snackbar
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter

class CreateAreaActivity : BaseActivity() {

    private lateinit var binding: ActivityCreateAreaBinding
    private lateinit var viewModel: CreateAreaViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_area)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)

        //viewModel = ViewModelProvider(this, MainViewModelFactory()).get(MainViewModel::class.java)
        viewModel = ViewModelProvider(this, ViewModelFactory(CreateAreaViewModel())).get(CreateAreaViewModel::class.java)
        binding.viewModel = this.viewModel

        binding.createAreaContent.createAreaType.setItems(
            getString(R.string.indoor),
            getString(R.string.outdoor),
            getString(R.string.mixed)
        )
        viewModel.getAreaType().value = getString(R.string.indoor)
        viewModel.getAreaTypeExplanation().value = getString(R.string.indoor_explanation)
        binding.createAreaContent.createAreaType.setOnItemSelectedListener(object: MaterialSpinner.OnItemSelectedListener<String> {
            override fun onItemSelected(
                view: MaterialSpinner?,
                position: Int,
                id: Long,
                item: String?
            ) {
                viewModel.getAreaType().value = item!!
                val typeExplanation = when (item) {
                    getString(R.string.outdoor) -> getString(R.string.outdoor_explanation)
                    getString(R.string.mixed) -> getString(R.string.mixed_explanation)
                    else -> getString(R.string.indoor_explanation)
                }
                viewModel.getAreaTypeExplanation().value = typeExplanation
            }
        })
        binding.executePendingBindings()

        viewModel.getSaveAttemptObservable().observe(this, Observer {
            if (it.success) {
                // Save successfull
                finish()
            } else {
                Snackbar.make(binding.root, it.message, Snackbar.LENGTH_LONG).show()
            }
        })
    }
}
