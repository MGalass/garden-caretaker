package com.gardencaretaker.ui.plantpairing.viewmodel

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PlantPairingViewModel: ViewModel() {

    private val TAG = "PlantPairingVM"

    private var plantIdFound = MutableLiveData<Boolean>()
    private var plantId = MutableLiveData<String>()

    private var acceptPlantObservable = MutableLiveData<String>()

    init {
        plantIdFound.value = false
        plantId.value = ""
    }

    fun isPlantIdFound() = plantIdFound

    fun getPlantId() = plantId
    fun setPlantId(plantId: String){
        this.plantId.value = plantId
        if (!TextUtils.isEmpty(this.plantId.value)) {
            plantIdFound.value = true
        }
    }
    fun getAcceptPlantObservable() = acceptPlantObservable

    fun retryScan() {
        plantIdFound.value = false
        plantId.value = ""
    }

    fun acceptPlantId() {
        // The user has accepted the scanned id as their new plant
        plantId.value?.let {
            acceptPlantObservable.postValue(it)
        }
    }


}