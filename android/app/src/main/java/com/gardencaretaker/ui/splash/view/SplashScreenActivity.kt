package com.gardencaretaker.ui.splash.view

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.gardencaretaker.ui.main.view.MainActivity
import com.gardencaretaker.R
import com.gardencaretaker.ui.base.view.BaseActivity
import com.gardencaretaker.databinding.ActivitySplashScreenBinding
import com.gardencaretaker.factory.ViewModelFactory
import com.gardencaretaker.ui.splash.viewmodel.SplashViewModel

class SplashScreenActivity : BaseActivity() {

    private lateinit var binding: ActivitySplashScreenBinding
    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Init UI databinding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen)
        binding.lifecycleOwner = this
        // Init viewmodel
        this.viewModel = ViewModelProvider(this,
            ViewModelFactory(SplashViewModel())
        ).get(SplashViewModel::class.java)

        binding.viewModel = this.viewModel
        this.viewModel.getAuthAttemptObservable().observe(this, Observer {
            if (it.success) {
                // Go to main
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                Snackbar.make(binding.root, it.message, Snackbar.LENGTH_LONG).show()
            }
        })
        binding.executePendingBindings()
    }

    override fun onStart() {
        super.onStart()
    }

}
