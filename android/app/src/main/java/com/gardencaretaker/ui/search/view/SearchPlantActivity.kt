package com.gardencaretaker.ui.search.view

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ActivitySearchPlantBinding
import com.gardencaretaker.factory.ViewModelFactory
import com.gardencaretaker.ui.base.view.BaseActivity
import com.gardencaretaker.ui.plant.view.PlantDetailActivity
import com.gardencaretaker.ui.search.adapter.viewmodel.SearchPlantResultsAdapter
import com.gardencaretaker.ui.search.viewmodel.SearchPlantViewModel

class SearchPlantActivity : BaseActivity() {

    companion object {
        const val KEY_AREA_ID = "keyAreaId"
        const val KEY_PLANT_ID = "keyPlantId"
    }

    private val MIN_CHARS_QUERY = 2

    private lateinit var binding: ActivitySearchPlantBinding
    private lateinit var viewModel: SearchPlantViewModel
    private lateinit var adapter: SearchPlantResultsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_plant)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val areaId = intent.extras?.getString(KEY_AREA_ID, null)
        if (TextUtils.isEmpty(areaId)) {
            finish()
            return
        }

        val plantId = intent.extras?.getString(KEY_PLANT_ID, null)
        if (TextUtils.isEmpty(plantId)) {
            finish()
            return
        }

        viewModel = ViewModelProvider(this, ViewModelFactory(SearchPlantViewModel())).get(SearchPlantViewModel::class.java)
        binding.viewModel = this.viewModel

        adapter = SearchPlantResultsAdapter(this, object : SearchPlantResultsAdapter.SearchPlantResultsAdapterListener{
            override fun onSearchPlantResultSelected(plantApiId: Int) {
                // Open the plant detail activity
                val intent = Intent(this@SearchPlantActivity, PlantDetailActivity::class.java)
                intent.putExtra(PlantDetailActivity.KEY_PLANT_ID, plantId)
                intent.putExtra(PlantDetailActivity.KEY_PLANT_API_ID, plantApiId)
                intent.putExtra(PlantDetailActivity.KEY_AREA_ID, areaId)
                startActivity(intent)
            }
        })
        binding.searchPlantContent.contentSearchPlantRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.searchPlantContent.contentSearchPlantRecyclerView.adapter = adapter

        // Add query listener to fetch plants from the API
        binding.searchPlantSearchview.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!TextUtils.isEmpty(query) && query!!.length > MIN_CHARS_QUERY) {
                    viewModel.updateSearchQuery(query)
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (!TextUtils.isEmpty(newText) && newText!!.length > MIN_CHARS_QUERY) {
                    viewModel.updateSearchQuery(newText)
                }
                return false
            }
        })

        viewModel.getSearchPaginatedResults().observe(this, Observer {
            // Propagating the adapter
            adapter.submitList(it)
        })

        // Observe search result and load the adapter with data
//        viewModel.getSearchPlantsResults().observe(this, Observer {
//            if (it.success) {
//                adapter.clear()
//                adapter.addSearchPlantResults(it.result!!)
//            } else {
//                Snackbar.make(binding.root, it.message, Snackbar.LENGTH_LONG).show()
//            }
//        })
    }

}
