package com.gardencaretaker.ui.search.adapter.viewmodel

import com.gardencaretaker.network.model.TreflePlantSearchResult

class ItemSearchPlantResultViewModel(private val searchPlantResult: TreflePlantSearchResult, private val listener: ItemSearchPlantResultViewModelListener?) {

    interface ItemSearchPlantResultViewModelListener {
        fun onSelectSearchPlantResult(plantApiId: Int)
    }

    fun getCommonName() = searchPlantResult.commonName
    fun getScientificName() = searchPlantResult.scientificName
    fun getCompleteData() = searchPlantResult.completeData

    fun selectSearchPlantResult() {
        // Propagating event on listener
        listener?.onSelectSearchPlantResult(searchPlantResult.id!!)
    }

}