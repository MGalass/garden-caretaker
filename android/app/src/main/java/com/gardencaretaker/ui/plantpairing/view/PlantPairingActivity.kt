package com.gardencaretaker.ui.plantpairing.view

import android.R.attr
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.JsonReader
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ActivityPlantPairingBinding
import com.gardencaretaker.factory.ViewModelFactory
import com.gardencaretaker.ui.base.view.BaseActivity
import com.gardencaretaker.ui.plantpairing.viewmodel.PlantPairingViewModel
import com.gardencaretaker.ui.search.view.SearchPlantActivity
import com.google.gson.JsonParser
import com.google.zxing.integration.android.IntentIntegrator
import java.lang.Exception


class PlantPairingActivity : BaseActivity() {

    private val TAG = "PlantPairingActivity"

    companion object {
        const val KEY_AREA_ID = "keyAreaId"
        private const val KEY_JSON_ID = "id"
    }

    private lateinit var binding: ActivityPlantPairingBinding
    private lateinit var viewModel: PlantPairingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_plant_pairing)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val areaId = intent.extras?.getString(SearchPlantActivity.KEY_AREA_ID, null)
        if (TextUtils.isEmpty(areaId)) {
            finish()
            return
        }

        viewModel = ViewModelProvider(this, ViewModelFactory(PlantPairingViewModel())).get(PlantPairingViewModel::class.java)
        binding.viewModel = this.viewModel

        viewModel.getAcceptPlantObservable().observe(this, Observer {
            // The user accepted the plant device, we can go to the plant selection
            goToSearchActivity(areaId!!, it)
        })

        binding.plantPairingContent.plantPairingContentScan.setOnClickListener {
            // Opening intent to qr scan
            val intentIntegrator = IntentIntegrator(this).apply {
                this.setPrompt(getString(R.string.scan_prompt))
                this.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
                this.setBeepEnabled(false)
                this.setBarcodeImageEnabled(true)
            }

            intentIntegrator.initiateScan();
        }

        binding.executePendingBindings()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents != null) {
                val scanContents = result.contents;
                // Parse the content as a JSON and retrieve the id
                Log.d(TAG, "Scan contents: $scanContents")
                val plantId = decodeScanContents(scanContents)
                // Set the plantId
                viewModel.setPlantId(plantId)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun decodeScanContents(scanContents: String?): String {
        if (TextUtils.isEmpty(scanContents)) {
            return ""
        } else {
            try {
                val json = JsonParser.parseString(scanContents!!).asJsonObject
                return json.get(KEY_JSON_ID).asString
            } catch (e: Exception) {
                Log.d(TAG, "Exception during id scan decoding: ${e.message}")
                return ""
            }
        }

    }

    private fun goToSearchActivity(areaId: String, plantId: String) {
        // Open SearchPlantActivity
        val intent = Intent(this, SearchPlantActivity::class.java)
        intent.putExtra(SearchPlantActivity.KEY_AREA_ID, areaId)
        intent.putExtra(SearchPlantActivity.KEY_PLANT_ID, plantId)
        startActivity(intent)
    }

}
