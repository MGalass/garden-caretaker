package com.gardencaretaker.ui.plant.view

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ActivityPlantDetailBinding
import com.gardencaretaker.factory.ViewModelFactory
import com.gardencaretaker.ui.base.view.BaseActivity
import com.gardencaretaker.ui.main.adapter.PlantsAdapter
import com.gardencaretaker.ui.main.view.MainActivity
import com.gardencaretaker.ui.plant.adapter.PlantImagesAdapter
import com.gardencaretaker.ui.plant.viewmodel.PlantDetailViewModel
import com.gardencaretaker.utils.setVisibleOrGone
import com.google.android.material.snackbar.Snackbar

class PlantDetailActivity : BaseActivity() {

    companion object {
        const val KEY_PLANT_ID = "keyPlantId"
        const val KEY_PLANT_API_ID = "keyPlantApiId"
        const val KEY_AREA_ID = "keyAreaId"
        const val KEY_SHOW_ONLY_MODE = "keyShowOnlyMode"
    }

    private lateinit var binding: ActivityPlantDetailBinding
    private lateinit var viewModel: PlantDetailViewModel
    private lateinit var adapter: PlantImagesAdapter

    private var showOnlyMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_plant_detail)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        showOnlyMode = intent.extras?.getBoolean(KEY_SHOW_ONLY_MODE, false)!!

        val plantApiId = intent.extras?.getInt(KEY_PLANT_API_ID, 0)
        if (plantApiId == 0) {
            finish()
            return
        }

        val plantId = intent.extras?.getString(KEY_PLANT_ID, "")
        if (TextUtils.isEmpty(plantId)) {
            finish()
            return
        }

        val areaId = intent.extras?.getString(KEY_AREA_ID, null)

        viewModel = ViewModelProvider(this, ViewModelFactory(PlantDetailViewModel(plantApiId!!, plantId, areaId, showOnlyMode))).get(PlantDetailViewModel::class.java)
        binding.viewModel = this.viewModel

        viewModel.getPlantInfo().observe(this, Observer {
            // When the plant is loaded, we load the images adapter (if present)
            it?.images?.let { plantImageList ->
                if (plantImageList.isNotEmpty()) {
                    adapter = PlantImagesAdapter(this@PlantDetailActivity)
                    val recyclerView = binding.contentPlantDetail.componentPlantImages.componentPlantImagesRecyclerView
                    recyclerView.layoutManager = LinearLayoutManager(this@PlantDetailActivity, RecyclerView.HORIZONTAL, false)
                    recyclerView.adapter = adapter
                    adapter.addPlantImages(plantImageList)
                }
            }
        })

        // Hide fab when scrolling down
        binding.contentPlantDetail.contentPlantDetailScrollview.setOnScrollChangeListener { _: NestedScrollView?, _: Int, scrollY: Int, _: Int, oldScrollY: Int ->
            binding.activityPlantDetailSelectPlant.setVisibleOrGone(scrollY < oldScrollY)
        }

        // Add the plant when the user clicks on the fab
        binding.activityPlantDetailSelectPlant.setOnClickListener {
            // Adding plant to area
            viewModel.addPlantToArea()
        }

        viewModel.getPlantAddedObservable().observe(this, Observer {
            if (it.success) {
                // The plant has been added, we can return to the Main Activity
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            } else {
                Snackbar.make(binding.root, it.message, Snackbar.LENGTH_LONG).show()
            }
        })

    }

}
