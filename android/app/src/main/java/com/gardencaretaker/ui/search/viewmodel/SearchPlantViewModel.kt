package com.gardencaretaker.ui.search.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.gardencaretaker.network.model.TreflePlantSearchResult
import com.gardencaretaker.network.services.TrefleDataSource
import com.gardencaretaker.network.services.TrefleDataSourceFactory
import com.gardencaretaker.utils.GenericResponse

class SearchPlantViewModel: ViewModel() {

    private val TAG = "SearchPlantVM"

    private val query = MutableLiveData<String>()
    private val searchByCommonName = MutableLiveData<Boolean>()
    private val searchByScientificName = MutableLiveData<Boolean>()
    private val completeDataOnly = MutableLiveData<Boolean>()

    private val searchPlantsResults = MutableLiveData<GenericResponse<List<TreflePlantSearchResult>>>()

    // Data source used to fetch paginated data
    private var trefleDataSourceFactory : TrefleDataSourceFactory
    private var trefleResultsDataSource : MutableLiveData<TrefleDataSource>
    private var treflePagedList: LiveData<PagedList<TreflePlantSearchResult>>

    init {
        searchByCommonName.value = true
        searchByScientificName.value = true
        completeDataOnly.value = true
        query.value = ""

        trefleDataSourceFactory = TrefleDataSourceFactory()
        trefleDataSourceFactory.searchParams = TrefleDataSource.SearchParams(
            completeDataOnly = completeDataOnly.value!!
        )
        trefleResultsDataSource = trefleDataSourceFactory.getDataSourceLiveData()

        // Config the pagedList, with the same pages as the one set in the TrefleDataSource
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setPageSize(TrefleDataSource.PAGE_SIZE)
            .build()

        // Building and connecting the paged list
        treflePagedList = LivePagedListBuilder(trefleDataSourceFactory, pagedListConfig).build()

    }

    fun getSearchPaginatedResults() = treflePagedList

    fun toggleCommonName() {
        searchByCommonName.value = !searchByCommonName.value!!
        searchPlants()
    }

    fun toggleScientificName() {
        searchByScientificName.value = !searchByScientificName.value!!
        searchPlants()
    }

    fun toggleOnlyCompleteData() {
        completeDataOnly.value = !completeDataOnly.value!!
        searchPlants()
    }

    fun updateSearchQuery(newQuery: String) {
        query.value = newQuery
        searchPlants()
    }

    private fun searchPlants() {
        trefleResultsDataSource.value?.let {

            val byCommonName = searchByCommonName.value!!
            val byScientificName = searchByScientificName.value!!
            val query = query.value!!

            // Updating params and invalidate the source
            trefleDataSourceFactory.searchParams = TrefleDataSource.SearchParams(
                query = if (byCommonName && byScientificName) query else null,
                scientificNameQuery = if (byScientificName && !byCommonName) query else null,
                commonNameQuery = if (!byScientificName && byCommonName) query else null,
                completeDataOnly = completeDataOnly.value!!
            )
            it.invalidate()
        }
    }

}