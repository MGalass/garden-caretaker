package com.gardencaretaker.ui.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    private val TAG = "MainVM"

    private var noAreas = MutableLiveData<Boolean>()

    init {
        noAreas.value = false
    }

    fun getNoAreas() = noAreas

}