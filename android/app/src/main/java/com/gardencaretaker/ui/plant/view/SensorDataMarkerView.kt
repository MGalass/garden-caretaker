package com.gardencaretaker.ui.plant.view

import android.content.Context
import androidx.databinding.DataBindingUtil
import com.gardencaretaker.R
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.google.android.material.textview.MaterialTextView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class SensorDataMarkerView : MarkerView {

    data class SensorDataMarkerInfo(val timestamp: String, val value: Float)

    private val dateTextView: MaterialTextView
    private val valueTextView: MaterialTextView

    constructor(context: Context?, layoutResource: Int) : super(context, layoutResource) {
        dateTextView = this.findViewById(R.id.item_data_entry_date)
        valueTextView = this.findViewById(R.id.item_data_entry_value)
    }

    override fun refreshContent(entry: Entry?, highlight: Highlight?) {
        super.refreshContent(entry, highlight)

        //Retrieve data
        val data = entry?.data as SensorDataMarkerInfo
        dateTextView.text = data.timestamp
        valueTextView.text = data.value.toString()
    }
}