package com.gardencaretaker.ui.main.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.gardencaretaker.R
import com.gardencaretaker.databinding.ItemAreaBinding
import com.gardencaretaker.model.Area
import com.gardencaretaker.model.Plant
import com.gardencaretaker.storage.FirebaseDatabaseManager
import com.gardencaretaker.storage.UserPreferences
import com.gardencaretaker.storage.model.FBUserArea
import com.gardencaretaker.ui.main.adapter.viewmodel.ItemAreaViewModel

class AreasAdapter(val lifecycleOwner: LifecycleOwner?, val listener: AreasAdapterListener?) : FirestoreRecyclerAdapter<Area, AreasAdapter.AreaViewHolder>(
    FirestoreRecyclerOptions.Builder<Area>()
        .setLifecycleOwner(lifecycleOwner)
        .setQuery(
            //FirebaseDatabaseManager.areasReference()
            FirebaseDatabaseManager.getUserAreasQuery(UserPreferences.getUserId())
        ) {
            Log.d("AreasAdapter", "Snapshot: $it")
            val fbUserArea = it.toObject(FBUserArea::class.java)!!
            Log.d("AreasAdapter", "Retrieve fbUserArea: $fbUserArea")
            val area = Area.fromFBUserArea(fbUserArea)
            Log.d("AreasAdapter", "Retrieve area: $area")
            area
        }
        .build()
){

    override fun onDataChanged() {
        super.onDataChanged()
        // Check the size and update ui if empty
        listener?.onDataChange(itemCount)
    }

    private val itemAreaViewModelListener = object: ItemAreaViewModel.ItemAreaViewModelListener {
        override fun onAddPlantToAreaEvent(areaId: String) {
            // Propagating event
            listener?.onAddPlantRequest(areaId)
        }
    }

    interface AreasAdapterListener {
        fun onAddPlantRequest(areaId: String)
        fun onViewPlantSensorDataRequest(plant: Plant)
        fun onDataChange(itemsCount: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AreaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_area, parent, false)
        // Passing the reference of the listener
        return AreaViewHolder(lifecycleOwner, view, areasAdapterListener = listener)
    }

    override fun onBindViewHolder(holder: AreaViewHolder, position: Int, area: Area) {
        if (position < itemCount) {
            // Creating item ViewModel with listener
            holder.getBinding().viewModel = ItemAreaViewModel(area, itemAreaViewModelListener)
            holder.getBinding().lifecycleOwner = lifecycleOwner
            holder.addPlantsToAdapter(area.id!!, area.plants ?: ArrayList())
            holder.getBinding().executePendingBindings()
        }
    }

    class AreaViewHolder(val lifecycleOwner: LifecycleOwner?, itemView: View, areasAdapterListener: AreasAdapterListener? = null) : RecyclerView.ViewHolder(itemView) {

        private var binding: ItemAreaBinding

        private val plantAdapterListener = object : PlantsAdapter.PlantsAdapterListener {
            override fun onViewPlantSensorDataRequest(plant: Plant) {
                // Propagating event through the AreasAdapterListener instance
                areasAdapterListener?.onViewPlantSensorDataRequest(plant)
            }
        }

        init {
            binding = DataBindingUtil.bind(itemView)!!

            binding.itemAreaPlants.layoutManager = LinearLayoutManager(itemView.context)
        }

        fun getBinding() = binding

        fun addPlantsToAdapter(areaId: String, plants: List<Plant>) {
            val adapter = PlantsAdapter(lifecycleOwner, areaId = areaId, listener = plantAdapterListener)
            adapter.addPlants(plants)
            // Add adapter to RecyclerView
            binding.itemAreaPlants.adapter = adapter
        }
    }
}