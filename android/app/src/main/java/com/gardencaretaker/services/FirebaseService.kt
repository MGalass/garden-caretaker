package com.gardencaretaker.services

import android.util.Log
import androidx.core.app.NotificationCompat
import com.gardencaretaker.R
import com.gardencaretaker.gardenCaretaker
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseService: FirebaseMessagingService() {

    companion object {
        const val TAG = "FirebaseService"
    }


    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d(TAG, "Received message from: ${remoteMessage.from}")
        Log.d(TAG, "Message data(): ${remoteMessage.data}")

        remoteMessage.notification?.let {
            Log.d(TAG, "Notification title: ${it.title}")
            Log.d(TAG, "Notification body: ${it.body}")

            // Firing the notification if the app is in foreground
            if (it.title != null && it.body != null) {
                gardenCaretaker().fireWarningNotification(it.title!!, it.body!!)
            }

        }
    }

}