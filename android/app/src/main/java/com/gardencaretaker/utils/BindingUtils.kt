package com.gardencaretaker.utils

import android.content.res.ColorStateList
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputLayout
import kotlin.math.round


@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

@BindingAdapter("visible")
fun View.setVisible(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.INVISIBLE
}

@BindingAdapter("toggleColor")
fun TextInputLayout.toggleColor(colorRes : Int) {
    setEndIconTintList(ColorStateList.valueOf(colorRes))
}

@BindingAdapter("imageUrl")
fun ImageView.imageUrl(url : String) {
    Glide
        .with(this)
        .load(url)
        .centerCrop()
        .into(this)
}

@BindingAdapter("plantInfoValue")
fun TextView.plantInfoValue(value : Any?) {
    if (value == null) {
        return
    }

    text = when (value) {
        null -> ""
        is Int -> value.toString()
        is Float -> round(value).toString()
        is Double -> round(value).toString()
        is String -> value
        else -> value.toString()
    }
}