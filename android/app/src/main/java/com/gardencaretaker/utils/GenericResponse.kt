package com.gardencaretaker.utils

data class GenericResponse<T>(val success: Boolean = true, val result: T? = null, val message: String = "")