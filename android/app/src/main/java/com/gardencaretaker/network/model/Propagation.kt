package com.gardencaretaker.network.model

data class Propagation (
	val bareRoot : Boolean? = null,
	val bulbs : Boolean? = null,
	val container : Boolean? = null,
	val corms : Boolean? = null,
	val cuttings : Boolean? = null,
	val seed : Boolean? = null,
	val sod : Boolean? = null,
	val sprigs : Boolean? = null,
	val tubers : Boolean? = null
)