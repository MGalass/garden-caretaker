package com.gardencaretaker.network.model

data class PlantHeight (
	val cm : Double? = null,
	val ft : Double? = null
)