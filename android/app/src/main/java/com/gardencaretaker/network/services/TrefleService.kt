package com.gardencaretaker.network.services

import android.util.Log
import com.gardencaretaker.BuildConfig
import com.gardencaretaker.network.api.TrefleApi
import com.gardencaretaker.network.model.TreflePlant
import com.gardencaretaker.network.model.TreflePlantSearchResult
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object TrefleService {

    private val TAG = "TrefleService"

    private val API_URL = "https://trefle.io/api/"

    private var okHttpClient: OkHttpClient? = null
    private var retrofit: Retrofit? = null

    private fun getHttpClient(): OkHttpClient {
        if (okHttpClient == null) {
            // Logging interceptor
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val trefleAuthInterceptor = Interceptor {
                val originalRequest = it.request()

                val newRequest = originalRequest.newBuilder()
                    .addHeader("Authorization", "Bearer ${BuildConfig.TREFLE_TOKEN}")
                    .build()

                val response = it.proceed(newRequest)
                response
            }
            // Create okHttpClient
            okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(trefleAuthInterceptor)
                .build()
        }
        return okHttpClient!!
    }

    private fun getRetrofit(): Retrofit {
        if (retrofit == null) {

            val gson = Gson().newBuilder()
                .setFieldNamingStrategy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setPrettyPrinting()
                .serializeNulls()
                .create()

            retrofit = Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(getHttpClient())
                .build()
        }
        return retrofit!!
    }

    private fun getApi(): TrefleApi {
        return getRetrofit().create(TrefleApi::class.java)
    }

    fun searchPlants(
        query: String? = null,
        scientificNameQuery: String? = null,
        commonNameQuery: String? = null,
        completeDataOnly: Boolean? = null,
        page: Int? = null,
        pageSize: Int? = null
    ): Task<List<TreflePlantSearchResult>> {
        val task = TaskCompletionSource<List<TreflePlantSearchResult>>()
        getApi().searchPlants(
            query = query,
            scientificNameQuery = scientificNameQuery,
            commonNameQuery = commonNameQuery,
            completeDataOnly = completeDataOnly,
            page = page,
            pageSize = pageSize
        ).enqueue(object : Callback<List<TreflePlantSearchResult>> {
            override fun onFailure(call: Call<List<TreflePlantSearchResult>>, t: Throwable) {
                Log.d(TAG, "Search failed: ${t.message}")
                task.setException(Exception(t))
            }
            override fun onResponse(
                call: Call<List<TreflePlantSearchResult>>,
                response: Response<List<TreflePlantSearchResult>>
            ) {
                if (response.isSuccessful) {
                    Log.d(TAG, "Search success")
                    task.setResult(response.body())
                } else {
                    Log.d(TAG, "Search failed: ${response.errorBody().toString()}")
                    task.setException(Exception(response.message()))
                }
            }
        })
        return task.task
    }

    fun searchPlant(plantId: Int): Task<TreflePlant> {
        val task = TaskCompletionSource<TreflePlant>()
        getApi().searchPlant(plantId).enqueue(object : Callback<TreflePlant> {
            override fun onFailure(call: Call<TreflePlant>, t: Throwable) {
                Log.d(TAG, "Search failed: ${t.message}")
                task.setException(Exception(t))
            }
            override fun onResponse(
                call: Call<TreflePlant>,
                response: Response<TreflePlant>
            ) {
                if (response.isSuccessful) {
                    Log.d(TAG, "Search success")
                    task.setResult(response.body())
                } else {
                    Log.d(TAG, "Search failed: ${response.errorBody().toString()}")
                    task.setException(Exception(response.message()))
                }
            }
        })
        return task.task
    }

}