package com.gardencaretaker.network.model

data class FruitOrSeed (
	val color : String? = null,
	val conspicuous : String? = null,
	val seedAbundance : String? = null,
	val seedPeriodBegin : String? = null,
	val seedPeriodEnd : String? = null,
	val seedPersistence : Boolean? = null
)