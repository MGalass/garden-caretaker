package com.gardencaretaker.network.model

import com.google.gson.annotations.SerializedName

data class TreflePlant (
    @SerializedName("class")
	val plantClass : TrefleReference? = null,
    val commonName : String? = null,
    val cultivars : List<String>? = null,
    val division : TrefleReference? = null,
    val duration : String? = null,
    val family : TrefleReference? = null,
    val familyCommonName : String? = null,
    val forms : List<Form>? = null,
    val genus : TrefleReference? = null,
    val hybrids : List<String>? = null,
    val id : Int? = null,
    val images : List<PlantImage>? = null,
    val mainSpecies : MainSpecies? = null,
    val nativeStatus : String? = null,
    val order : TrefleReference? = null,
    val scientificName : String? = null,
    val subSpecies : List<SubSpecies>? = null,
    val varieties : List<Variety>? = null
)