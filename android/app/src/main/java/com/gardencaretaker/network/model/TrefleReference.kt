package com.gardencaretaker.network.model

data class TrefleReference (
	val commonName: String? = null,
	val id : Int? = null,
	val link : String? = null,
	val name : String? = null,
	val slug : String? = null
)