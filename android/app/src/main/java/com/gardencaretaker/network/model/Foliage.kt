package com.gardencaretaker.network.model

data class Foliage (
	val color : String? = null,
	val porositySummer : String? = null,
	val porosityWinter : String? = null,
	val texture : String? = null
)