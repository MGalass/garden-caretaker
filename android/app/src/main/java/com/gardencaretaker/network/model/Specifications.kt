package com.gardencaretaker.network.model

data class Specifications (
	val bloat : String? = null,
	val cNRatio : String? = null,
	val coppicePotential : String? = null,
	val fallConspicuous : String? = null,
	val fireResistance : String? = null,
	val growthForm : String? = null,
	val growthHabit : String? = null,
	val growthPeriod : String? = null,
	val growthRate : String? = null,
	val knownAllelopath : String? = null,
	val leafRetention : String? = null,
	val lifespan : String? = null,
	val lowGrowingGrass : String? = null,
	val matureHeight : PlantHeight? = null,
	val maxHeightAtBaseAge : PlantHeight? = null,
	val nitrogenFixation : String? = null,
	val regrowthRate : String? = null,
	val shapeAndOrientation : String? = null,
	val toxicity : String? = null
)