package com.gardencaretaker.network.services

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.gardencaretaker.network.model.TreflePlantSearchResult

/**
 * This data source will help to paginate results from the api calls
 */
class TrefleDataSource(val searchParams: SearchParams? = null): PageKeyedDataSource<Int, TreflePlantSearchResult>() {

    private val TAG = "TrefleDataSource"

    companion object {
        val PAGE_SIZE = 30
        val FIRST_PAGE = 1
    }

    data class SearchParams(
       val query: String? = null,
       val scientificNameQuery: String? = null,
       val commonNameQuery: String? = null,
       val completeDataOnly: Boolean? = null
    )

    // Called one on load
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, TreflePlantSearchResult>
    ) {
        TrefleService.searchPlants(
            query = searchParams?.query,
            scientificNameQuery = searchParams?.scientificNameQuery,
            commonNameQuery = searchParams?.commonNameQuery,
            completeDataOnly = searchParams?.completeDataOnly,
            page = FIRST_PAGE,
            pageSize = PAGE_SIZE
        ).addOnSuccessListener {
            // Load next page
            callback.onResult(it, null, FIRST_PAGE + 1)
        }.addOnFailureListener {
            // Manage error
        }
    }

    // This will load the next page
    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, TreflePlantSearchResult>
    ) {
        Log.d(TAG, "Loading data: ${params.key}")
        TrefleService.searchPlants(
            query = searchParams?.query,
            scientificNameQuery = searchParams?.scientificNameQuery,
            commonNameQuery = searchParams?.commonNameQuery,
            completeDataOnly = searchParams?.completeDataOnly,
            page = params.key,  // Using current page for pagination
            pageSize = PAGE_SIZE
        ).addOnSuccessListener {
            // Load next page
            val pageKey = if (it.size == PAGE_SIZE) params.key + 1 else null
            Log.d(TAG, "NEXT PAGE: ${pageKey}")
            callback.onResult(it, pageKey)
        }.addOnFailureListener {
            // Manage error
        }
    }

    // This will load the previous page
    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, TreflePlantSearchResult>
    ) {
        // Currently not implemented in our model
    }
}