package com.gardencaretaker.network.model

data class SoilsAdaptation (
	val coarse : Boolean? = null,
	val fine : Boolean? = null,
	val medium : Boolean? = null
)