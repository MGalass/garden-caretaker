package com.gardencaretaker.network.model

data class Temperature (
	val degC : Double? = null,
	val degF : Double? = null
)