package com.gardencaretaker.network.services

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.gardencaretaker.network.model.TreflePlantSearchResult

class TrefleDataSourceFactory: DataSource.Factory<Int, TreflePlantSearchResult>() {

    private val dataSourceLiveData = MutableLiveData<TrefleDataSource>()

    var searchParams: TrefleDataSource.SearchParams? = TrefleDataSource.SearchParams()

    override fun create(): DataSource<Int, TreflePlantSearchResult> {
        // Creating the data source
        val dataSource = TrefleDataSource(searchParams)

        // Propagating the data source everytime it is created
        dataSourceLiveData.postValue(dataSource)

        return dataSource
    }

    fun getDataSourceLiveData() = dataSourceLiveData

}