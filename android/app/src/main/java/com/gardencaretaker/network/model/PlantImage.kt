package com.gardencaretaker.network.model

data class PlantImage (
	val url : String? = null
)