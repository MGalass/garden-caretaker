package com.gardencaretaker.network.model

data class PlantingDensity (
	val acre : Double? = null,
	val sqm : Double? = null
)