package com.gardencaretaker.network.model

data class Products (
	val berryNutSeed : String? = null,
	val christmasTree : String? = null,
	val fodder : String? = null,
	val fuelwood : String? = null,
	val lumber : String? = null,
	val navalStore : String? = null,
	val nurseryStock : String? = null,
	val palatableBrowseAnimal : String? = null,
	val palatableGrazeAnimal : String? = null,
	val palatableHuman : String? = null,
	val post : String? = null,
	val proteinPotential : String? = null,
	val pulpwood : String? = null,
	val veneer : String? = null
)