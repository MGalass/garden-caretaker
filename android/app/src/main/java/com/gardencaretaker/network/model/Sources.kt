package com.gardencaretaker.network.model

data class Sources (
	val lastUpdate : String? = null,
	val name : String? = null,
	val sourceUrl : String? = null,
	val speciesId : Int? = null
)