package com.gardencaretaker.network.model

data class Variety (
    val author : String,
    val bibliography : String,
    val commonName : String,
    val completeData : Boolean,
    val familyCommonName : String,
    val id : Int,
    val isMainSpecies : Boolean,
    val link : String,
    val mainSpeciesId : Int,
    val scientificName : String,
    val slug : String,
    val sources : List<Sources>,
    val status : String,
    val synonym : Boolean,
    val type : String,
    val year : String
)

typealias SubSpecies = Variety
typealias Form = Variety