package com.gardencaretaker.network.api

import com.gardencaretaker.network.model.TreflePlant
import com.gardencaretaker.network.model.TreflePlantSearchResult
import com.google.android.gms.common.internal.safeparcel.SafeParcelable
import com.squareup.okhttp.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TrefleApi {

    @GET("plants")
    fun searchPlants(
        @Query("q") query: String? = null,
        @Query("scientific_name") scientificNameQuery: String? = null,
        @Query("common_name") commonNameQuery: String? = null,
        @Query("complete_data") completeDataOnly: Boolean? = null,
        @Query("page") page: Int? = null,
        @Query("page_size") pageSize: Int? = null
    ): Call<List<TreflePlantSearchResult>>

    @GET("plants/{id}")
    fun searchPlant(
        @Path("id") id: Int
    ): Call<TreflePlant>

}