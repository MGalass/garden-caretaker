package com.gardencaretaker.network.model

data class TreflePlantSearchResult(
    val slug: String? = null,
    val scientificName: String? = null,
    val link: String? = null,
    val id: Int? = 0,
    val completeData: Boolean? = false,
    val commonName: String? = null
) {}
