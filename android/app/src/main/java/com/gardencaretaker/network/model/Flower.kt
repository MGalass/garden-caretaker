package com.gardencaretaker.network.model

data class Flower (
	val color : String? = null,
	val conspicuous : Boolean? = null
)