package com.gardencaretaker.network.model

data class RootDepth (
	val cm : Double? = null,
	val inches : Double? = null
)