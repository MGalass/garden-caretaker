package com.gardencaretaker.network.model

data class Seed (
	val bloomPeriod : String? = null,
	val commercialAvailability : String? = null,
	val seedSpreadRate : String? = null,
	val seedlingVigor : String? = null,
	val seedsPerPound : String? = null,
	val smallGrain : String? = null,
	val vegetativeSpreadRate : String? = null
)