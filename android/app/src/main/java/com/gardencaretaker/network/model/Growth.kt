package com.gardencaretaker.network.model

data class Growth (
	val anaerobicTolerance : String? = null,
	val caco3Tolerance : String? = null,
	val coldStratificationRequired : String? = null,
	val droughtTolerance : String? = null,
	val fertilityRequirement : String? = null,
	val fireTolerance : String? = null,
	val frostFreeDaysMinimum : Double? = null,
	val hedgeTolerance : String? = null,
	val moistureUse : String? = null,
	val phMaximum : Double? = null,
	val phMinimum : Double? = null,
	val plantingDensityMaximum : PlantingDensity? = null,
	val plantingDensityMinimum : PlantingDensity? = null,
	val precipitationMaximum : Precipitation? = null,
	val precipitationMinimum : Precipitation? = null,
	val resproutAbility : Boolean? = null,
	val rootDepthMinimum : RootDepth? = null,
	val salinityTolerance : String? = null,
	val shadeTolerance : String? = null,
	val temperatureMinimum : Temperature? = null
)