package com.gardencaretaker.network.model

data class Precipitation (
	val cm : Double? = null,
	val inches : Double? = null
)