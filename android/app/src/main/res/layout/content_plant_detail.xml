<?xml version="1.0" encoding="utf-8"?>
<layout>
    <data>
        <variable
            name="viewModel"
            type="com.gardencaretaker.ui.plant.viewmodel.PlantDetailViewModel" />
        <import type="android.text.TextUtils"/>
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:layout_behavior="@string/appbar_scrolling_view_behavior"
        tools:context=".ui.plant.view.PlantDetailActivity"
        tools:showIn="@layout/activity_plant_detail">

        <androidx.core.widget.NestedScrollView
            android:id="@+id/content_plant_detail_scrollview"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintBottom_toBottomOf="parent">

            <com.google.android.material.card.MaterialCardView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="@dimen/s">

                <FrameLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    visibleOrGone="@{viewModel.isLoading() || viewModel.isFailed()}">

                    <com.google.android.material.textview.MaterialTextView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        style="@style/TextAppearance.AppCompat.Caption"
                        android:textSize="18sp"
                        android:text="@string/error_retrieve_plant"
                        android:fontFamily="@font/alegreya_sans_medium"
                        android:textColor="@android:color/black"
                        android:textAlignment="center"
                        android:layout_margin="@dimen/xl"
                        visibleOrGone="@{viewModel.isFailed()}"/>

                    <ProgressBar
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_gravity="center"
                        visibleOrGone="@{viewModel.isLoading()}"/>

                </FrameLayout>

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="vertical"
                    android:layout_margin="@dimen/xl"
                    visibleOrGone="@{!(viewModel.isLoading() || viewModel.isFailed())}">

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:orientation="horizontal"
                        visibleOrGone="@{viewModel.plantInfo.mainSpecies.completeData}">

                        <ImageView
                            android:layout_width="18sp"
                            android:layout_height="18sp"
                            android:src="@drawable/ic_stars_black_24dp"
                            android:tint="@color/successColor"/>

                        <com.google.android.material.textview.MaterialTextView
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            style="@style/TextAppearance.AppCompat.Caption"
                            android:text="@string/complete_data"
                            android:fontFamily="@font/alegreya_sans_medium"
                            android:textColor="@color/successColor"
                            android:layout_marginStart="@dimen/m"/>

                    </LinearLayout>

                    <com.google.android.material.textview.MaterialTextView
                        android:id="@+id/content_plant_detail_scientific_name"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        style="@style/TextAppearance.AppCompat.Caption"
                        android:textSize="24sp"
                        android:text="@{viewModel.plantInfo.scientificName}"
                        android:fontFamily="@font/alegreya_sans_medium"
                        android:textColor="@android:color/black" />

                    <com.google.android.material.textview.MaterialTextView
                        android:id="@+id/content_plant_detail_common_name"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        style="@style/TextAppearance.AppCompat.Caption"
                        android:textSize="18sp"
                        android:text="@{viewModel.plantInfo.commonName}"
                        android:fontFamily="@font/alegreya_sans_medium"
                        android:textColor="@android:color/black"
                        visibleOrGone="@{!TextUtils.isEmpty(viewModel.plantInfo.commonName)}" />

                    <include layout="@layout/component_plant_images"
                        android:id="@+id/component_plant_images"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:plant="@{viewModel.plantInfo}"
                        />

                    <include layout="@layout/component_phylogenetic_tree"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:plant="@{viewModel.plantInfo}" />

                    <include layout="@layout/component_main_species"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:mainSpecies="@{viewModel.plantInfo.mainSpecies}" />

                    <include layout="@layout/component_foliage"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:foliage="@{viewModel.plantInfo.mainSpecies.foliage}" />

                    <include layout="@layout/component_flower"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:flower="@{viewModel.plantInfo.mainSpecies.flower}" />

                    <include layout="@layout/component_fruit_or_seed"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:fruitOrSeed="@{viewModel.plantInfo.mainSpecies.fruitOrSeed}"
                        app:seed="@{viewModel.plantInfo.mainSpecies.seed}"/>

                    <include layout="@layout/component_soil_adaptation"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:soilsAdaptation="@{viewModel.plantInfo.mainSpecies.soilsAdaptation}" />

                    <include layout="@layout/component_propagation"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:propagation="@{viewModel.plantInfo.mainSpecies.propagation}" />

                    <include layout="@layout/component_plant_growth"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/l"
                        app:growth="@{viewModel.plantInfo.mainSpecies.growth}" />

                </LinearLayout>

            </com.google.android.material.card.MaterialCardView>

        </androidx.core.widget.NestedScrollView>

    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
