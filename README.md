# README #

Garden Caretaker lets you control and supervise your lovely plants condition remotely with your smartphone.

## Setup instructions

### Arduino

1. Download [Arduino IDE](https://www.arduino.cc/en/main/software) 
2. Open the `arduino` folder in Arduino IDE
3. Compile the code
4. Load the code into your Arduino device

### Raspberry PI

1. Download and install the latest [Buster OS](https://www.raspberrypi.org/downloads/raspberry-pi-os/) on your Raspberry PI 
2. Update and upgrade your distro: `sudo apt update && sudo apt upgrade`
3. Install Python 3 (if not already present)
4. Install [Poetry](https://python-poetry.org/)
5. Open a terminal and navigate to the `python` folder
6. Place inside `config` folder a json file called `config.json` containing the device/plant id and the serial port path:

```
	{
		'id': '1wUqvcrtASSRRz8K4VH2',
		'serialPort': '/dev/ttyACM0'
	}
```

7. Place inside `certs` folder a json file called `garden-caretaker-key.json`, which may not be tracked by this repository
8. Run `poetry install` to download the dependencies
9. Run `poetry run python3 src/main.py` to start the program. The Arduino device must be connected beforehand

### Cloud Functions

1. Download and install [Node.js](https://nodejs.org/it/) (version > 8)
2. Navigate to the `node\functions` folder
3. Install the dependencies: `npm i`
4. Install the [Firebase CLI](https://github.com/firebase/firebase-tools) with global scope: `npm install -g firebase-tools`
5. Deploy your functions: `firebase deploy --only functions`

### Android

1. Download and install [Android Studio](https://developer.android.com/studio)
2. Import the project placed inside `android` folder
3. Download Kotlin support (if not already present)
4. Build the project and run on your Android device

