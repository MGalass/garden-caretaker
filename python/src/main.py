import requests
import os
import json
import RPi.GPIO as GPIO

from garden_caretaker import GardenCaretaker

LED_PIN = 21

response = requests.get('https://httpbin.org/ip')
print('Your IP is {0}'.format(response.json()['origin']))

print(os.getcwd())

# Retrieve system configuration
with open("config/config.json") as configFile:
    config = json.load(configFile)
    
print(config)

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "certs/garden-caretaker-key.json"
os.environ['GRPC_DNS_RESOLVER'] = 'native'

GPIO.setwarnings(False)
GPIO.cleanup()
GPIO.setmode(GPIO.BCM)

garden_caretaker = GardenCaretaker(config["serialPort"], config["id"], LED_PIN)
garden_caretaker.run()



