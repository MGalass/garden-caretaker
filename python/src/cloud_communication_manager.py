import datetime
import time

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

import RPi.GPIO as GPIO

class CloudCommunicationManager:
    
    # Collections name
    BRIGHTNESS = "brightness"
    TEMPERATURE = "temperature"
    SOIL_HUMIDITY = "soilHumidity"
    RAINFALL = "rainfall"
    
    def __init__(self, plant_id):
        self.plant_id = plant_id
        # Init Firestore        
        cred = credentials.ApplicationDefault()
        firebase_admin.initialize_app()
        self.db = firestore.Client()
        # Creating callback field for active flag
        self.last_active_value = None
        self.active_flag_callback = None
        print("Finished initialization for client with id: " + self.plant_id);
        
    def add_plant_sensor_data(self, collection, value):
        # Retrieve current timestamp and add a document in the requested collection
        timestamp = int(round(time.time()))
        val = float(value)
        plant_ref = self.db.collection(u'plants').document(self.plant_id)
        plant_collection_ref = plant_ref.collection(collection)
        doc = plant_collection_ref.document(str(timestamp))
        doc.set(self.create_sensor_data(timestamp, val))
        # Set the last value in a document field
        lastValueJson = {}
        lastValueJson[collection + 'LastValue'] = val
        plant_ref.update(lastValueJson)
        
    def create_sensor_data(self, timestamp, value):
        return {u'timestamp': timestamp, u'value': value}
     
    def update_brightness(self, value):
        self.add_plant_sensor_data(self.BRIGHTNESS, value)

    def update_temperature(self, value):
        self.add_plant_sensor_data(self.TEMPERATURE, value)

    def update_soil_humidity(self, value):
        self.add_plant_sensor_data(self.SOIL_HUMIDITY, value)

    def update_rainfall(self, value):
        self.add_plant_sensor_data(self.RAINFALL, value)

    def on_document_change_callback(self, doc_snapshot, changes, read_time):
        doc_data = doc_snapshot[0].to_dict()
        active = doc_data['active']
        if (active != self.last_active_value) :
            print(f'Active value changed to {active}')
            self.last_active_value = active
            # Propagate the data to the callback
            if (self.active_flag_callback != None) :
                self.active_flag_callback(self.last_active_value)

    def set_active_flag_callback(self, active_flag_callback):
        self.active_flag_callback = active_flag_callback
        plant_ref = self.db.collection(u'plants').document(self.plant_id)
        plant_ref.on_snapshot(self.on_document_change_callback)