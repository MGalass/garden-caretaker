from base_led import BaseLed

import RPi.GPIO as GPIO

class Led(BaseLed):
    def __init__(self, pin):
        super().__init__(pin)
        GPIO.setup(pin, GPIO.OUT)
        
    def switch_on(self):
        super().switch_on()
        GPIO.output(self.pin, GPIO.HIGH)
        
    def switch_off(self):
        super().switch_off()
        GPIO.output(self.pin, GPIO.LOW)
