import os

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

from serial_communication_manager import SerialCommunicationManager
from cloud_communication_manager import CloudCommunicationManager

import RPi.GPIO as GPIO
from led import Led

class GardenCaretaker:
    def __init__(self, serial_port, plant_id, led_pin):
        self.serial_communication_manager = SerialCommunicationManager(serial_port)
        self.cloud_communication_manager = CloudCommunicationManager(plant_id)
        self.active = False
        self.led = Led(led_pin)

    def on_active_status_change(self, active):
        self.active = active
        print(f'Device active status: {self.active}')
        if (self.active) :
            print(f'The device will collect data from sensors and send them to the cloud')
            self.led.switch_on()
        else:
            print(f'The device will not collect data from sensors')
            self.led.switch_off()    
        
    def on_message_from_serial(self, message):
        print(message)
        if (self.active):
            # Send data to cloud if and only if the device is active
            code = message[:1]
            value = message[1:]
            if code == 'B':
                self.cloud_communication_manager.update_brightness(value)
            if code == 'T':
                self.cloud_communication_manager.update_temperature(value)
            if code == 'S':
                self.cloud_communication_manager.update_soil_humidity(value)
            if code == 'R':
                self.cloud_communication_manager.update_rainfall(value)

    def run(self):
        # Executing callback on active flag change
        self.cloud_communication_manager.set_active_flag_callback(self.on_active_status_change)
        # Executing callback on new message
        self.serial_communication_manager.read(self.on_message_from_serial)        
