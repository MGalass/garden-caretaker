class BaseLed:
    def __init__(self, pin):
        self.pin = pin
        print(f"LED connected at pin {pin}")

    def switch_on(self):
        print("LED switched on")
        
    def switch_off(self):
        print("LED switched off")