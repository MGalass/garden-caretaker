import serial
import time

import RPi.GPIO as GPIO

from serial.serialutil import SerialException

class SerialCommunicationManager:
    def __init__(self, serialPort, rate = 9600):
        self.serialPort = serialPort
        self.rate = rate
        
    def read(self, on_message):
        try:  
            ser = serial.Serial(self.serialPort, self.rate, timeout = 1)
            ser.flush()
            while True:
                if ser.in_waiting > 0 :
                    line = ser.readline().decode('utf-8').rstrip()
                    on_message(line)
                # Let other thread to use the CPU
                time.sleep(0.01)
        except KeyboardInterrupt:
            print("User interrupted the application")
            GPIO.cleanup()
        except SerialException:
            print("Could not connect to " + self.serialPort)
            GPIO.cleanup()

